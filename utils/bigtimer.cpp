#include "bigtimer.h"
#include "QDebug"

BigTimer* BigTimer::singleton = nullptr;

BigTimer *BigTimer::getInstance()
{
    if (!singleton) singleton = new BigTimer;
    return singleton;
}

void BigTimer::start()
{
    timer.start();
}

void BigTimer::stop()
{
    totalTime += timer.nsecsElapsed();
    looped++;
}

double BigTimer::printAverageTime()
{
    double average = 0;
    if(looped>0){
        average = (totalTime / looped) / 1000000.0;  // conversion en millisecondes
        average = (int)(average * 10000 + .5);              // pour avoir 4 décimales
        average = (double)average / 10000;                  // pour avoir 4 décimales
    }
    qDebug() << average << "ms";
    return average;
}
