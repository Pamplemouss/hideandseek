#ifndef BIGTIMER_H
#define BIGTIMER_H

#include <QElapsedTimer>


class BigTimer
{
public:
    static BigTimer* getInstance();
    void start();
    void stop();
    double printAverageTime();
private:
    static BigTimer* singleton;
    QElapsedTimer timer;
    double totalTime = 0;
    double looped = 0;
};

#endif // BIGTIMER_H
