#ifndef UTILS_H
#define UTILS_H

#include <QColor>
#include <QVector>



class Utils
{
public:
    Utils();
    static int randomExclude(int max, const QVector<int> exclude = QVector<int>());
    static QColor blendColors(const QColor &c1, const QColor &c2, double r=0.5);
};

#endif // UTILS_H
