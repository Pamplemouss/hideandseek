#include "utils/random.h"
#include "utils/utils.h"


Utils::Utils()
{

}

int Utils::randomExclude(int max, const QVector<int> exclude)
{
    Random r;
    int res;
    do{
        res = r.i(max);
    }while(exclude.contains(res));

    return res;
}

QColor Utils::blendColors(const QColor &color1, const QColor &color2, double r)
{
    return QColor(
        color1.red() * (1-r) + color2.red()*r,
        color1.green()* (1-r) + color2.green()*r,
        color1.blue()* (1-r) + color2.blue()*r,
        255);
}
