#ifndef FILE_GRAND_H_INCLUDED
#define FILE_GRAND_H_INCLUDED

#include <QRandomGenerator>

class Random{
public:
    Random():g(QRandomGenerator::global()){

    }
    ~Random(){
        if(g!=QRandomGenerator::global()){
            delete g;
        }
    }
    void seed(int s){
        if(g!=QRandomGenerator::global()){
            delete g;
        }
        g = new QRandomGenerator(s);
    }
    int i(int max=2){
        return g->bounded(max);
    }

    double d(double max=1.0){
        double a = g->generateDouble()*max;
        return a;
    }

    bool b(){
        return this->i(2)==0;
    }

private:
    QRandomGenerator *g;
};

#endif
