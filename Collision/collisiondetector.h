#ifndef COLLISIONDETECTOR_H
#define COLLISIONDETECTOR_H

#include <QLineF>
#include <QRectF>


class Entity;

class CollisionDetector
{
public:
    static bool lineCollision(const QLineF &line, const QRectF &box);
private:
    typedef int OutCode;

    static const int INSIDE = 0; // 0000
    static const int LEFT = 1;   // 0001
    static const int RIGHT = 2;  // 0010
    static const int BOTTOM = 4; // 0100
    static const int TOP = 8;    // 1000

    static OutCode ComputeOutCode(double x, double y, int xmin, int xmax, int ymin, int ymax);

};

#endif // COLLISIONDETECTOR_H
