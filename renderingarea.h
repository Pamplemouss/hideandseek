#ifndef RENDERINGAREA_H
#define RENDERINGAREA_H
#include "Game/gamemanager.h"
#include "hiderfactory.h"
#include "seekerfactory.h"


#include <QMutex>
#include <QOpenGLWidget>

#include <Training/genetictrainer.h>
#include <Training/opponentstrainer.h>

#include <AI/hiderai.h>
#include <AI/seekerai.h>

#include <scorehistory.h>

class RenderingArea : public QOpenGLWidget
{
    Q_OBJECT
public:
    RenderingArea(QWidget *parent);
    ~RenderingArea();

    // QOpenGLWidget interface
protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();


private:
    /*
    QTimer *timer;
    QPainter *painter;
    Qt::Key seekerLastDirKey;
    Qt::Key hiderLastDirKey;
    GameManager gameManager;
    */
    GameManager *displayGame;

    HiderAI * bestHider;
    SeekerAI *bestSeeker;

    QTimer *timer;
    QTimer *gameTick;

    QTimer *restartTimer;
    QPainter *painter;
    QMutex mutex;
    GeneticTrainer *hidersTrainer;
    GeneticTrainer *seekersTrainer;

    OpponentsTrainer *trainer;

    HiderFactory *hiderFactory;
    SeekerFactory *seekerFactory;
    double bestHiderScore;
    double bestSeekerScore;

    bool trainHiders;
    bool render;
    int trainingCycles;
    double lastBestFitness;
    int  nPreview;
    int trainingCyclesPerGroup;
    int  totalTrainingCycles;
    ScoreHistory scoreHistory;


public slots:
    void restartGame();
    void step();

    void onNewBestScore(Trainable *t1, Trainable *t2);

};

#endif // RENDERINGAREA_H
