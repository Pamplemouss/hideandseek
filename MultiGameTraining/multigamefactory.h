#ifndef MULTIGAMESEEKERFACTORY_H
#define MULTIGAMESEEKERFACTORY_H

#include "multigametrainable.h"

#include <Training/abstracttrainablefactory.h>

#include <Game/gamemanager.h>


template <class ElementType, class OpponentType>
class MultigameFactory: public AbstractTrainableFactory
{
public:
    MultigameFactory()
    {

    }

    // AbstractTrainableFactory interface
public:
    Trainable *create(const QString &name) const{
        MultiGameTrainable<ElementType> *res = new MultiGameTrainable<ElementType>();
        QVector<ElementType*> games;

        for(auto *opponent : opponents){
            ElementType *newElement = new ElementType(name);
            initGame(newElement, dynamic_cast<OpponentType*>(opponent));

            games.append(newElement);
        }
        res->setSessions(games);

        return res;
    }
    Trainable *create(const Trainable &trainable) const{
        MultiGameTrainable<ElementType> *res = new MultiGameTrainable<ElementType>();
        QVector<ElementType*> games;

        for(auto *opponent : opponents){
            ElementType *newElement = new ElementType(*(dynamic_cast<const MultiGameTrainable<ElementType>&>(trainable).getAI()));
            initGame(newElement, dynamic_cast<OpponentType*>(opponent));

            games.append(newElement);
        }
        res->setSessions(games);

        return res;
    }

    Trainable *copy(const Trainable &trainable) const{
        MultiGameTrainable<ElementType> *res = new MultiGameTrainable<ElementType>();
        const MultiGameTrainable<ElementType> *multiTrainable = dynamic_cast<const MultiGameTrainable<ElementType>*>(&trainable);

        QVector<ElementType*> games;

        for(ElementType *session : multiTrainable->getSessions()){
            ElementType *newElement = new ElementType(*multiTrainable->getAI());
            OpponentType *opponent;
            GameManager *gameManager = session->getGameManager();
             Q_ASSERT_X(gameManager, "", "no gameManager ");

            // if we are a seeker, our opponent is a hider
            if(dynamic_cast<Seeker*>(newElement)){
                Q_ASSERT_X(gameManager->getHiders().size() > 0, "", "size < 1");
                opponent = dynamic_cast<OpponentType*>(session->getGameManager()->getHiders().first());
            }

            else if(dynamic_cast<Hider*>(newElement)){
                Q_ASSERT_X(gameManager->getSeekers().size() > 0, "", "size < 1");
                opponent = dynamic_cast<OpponentType*>(session->getGameManager()->getSeekers().first());
            }
            else{
                qFatal("Trainable *copy(const Trainable &trainable) Unknown opponent type");
            }

            Q_ASSERT_X(opponent, "Trainable *copy(const Trainable &trainable)", "Could not cast opponent");

            initGame(newElement, opponent);

            games.append(newElement);
        }
        res->setSessions(games);
        res->setScore(multiTrainable->getScore());
        return res;
    }

    void remove(Trainable *t) const{
        Q_ASSERT_X(dynamic_cast<MultiGameTrainable<ElementType>*>(t), "void remove()", "Trying to remove an  element that is not a MultiGameTrainable");

        MultiGameTrainable<ElementType> *s = dynamic_cast<MultiGameTrainable<ElementType>*>(t);
        for(ElementType *game : s->getSessions()){
            GameManager *g(game->getGameManager());
            qDeleteAll(g->getHiders());
            qDeleteAll(g->getSeekers());
            delete g;
        }


    }
    void setOpponents(const QVector<Trainable *> &value){
        qDeleteAll(opponents);
        opponents.clear();
        for(auto *v : value){
            // we may receive a OpponentType as opponent or a MultiGameTrainable
            if(dynamic_cast<OpponentType*>(v)){
                opponents.append(new OpponentType(*dynamic_cast<OpponentType*>(v)));
            }
            else if(dynamic_cast<MultiGameTrainable<OpponentType>*>(v)){
                opponents.append(new OpponentType(*dynamic_cast<MultiGameTrainable<OpponentType>*>(v)->getAI()));
            }
            else{
                qFatal("MultigameFactory::setOpponents(): Received an unknwon trainable as opponent");
            }
        }
    }

    int getDesiredOpponentCount() const{
        return 5;
    }

private:

    void initGame(ElementType* newElement, OpponentType *opponent)const{
        GameManager *g = new GameManager();

        // notify game manager of his new players
        // check type (not pretty)
        if(dynamic_cast<Seeker*>(newElement)){
            g->setSeekers({newElement});
            g->setHiders({new OpponentType(*opponent)});
        }
        else if(dynamic_cast<Hider*>(newElement)){
            g->setHiders({newElement});
            g->setSeekers({new OpponentType(*opponent)});
        }
        else{
            qFatal(" void initGame(ElementType* newElement, OpponentType *opponent): could not cast element");
        }

        g->restart();
    }


};

#endif // MULTIGAMESEEKERFACTORY_H
