#ifndef MULTIGAMESEEKER_H
#define MULTIGAMESEEKER_H

#include <Training/trainable.h>

#include <QVector>

#include <AI/hidenseekai.h>


template<class T>
class MultiGameTrainable : public Trainable
{
public:
    MultiGameTrainable():
        Trainable()
    {

    }
    MultiGameTrainable(const MultiGameTrainable<T> &other):
    Trainable(other)
    {
        for(const auto *s : other.sessions){
            sessions.append(new T(*s));
        }
    }
    virtual ~MultiGameTrainable()
    {
        qDeleteAll(sessions);
    }

    double getScore() const
    {
        double s=0;
        for(const auto &t : sessions){
            s+=t->getScore();
        }

        return sessions.size() == 0 ? 0 : s/sessions.size();
    }
    void setScore(double value)
    {
        for(auto &t : sessions){
            t->setScore(value);
        }

        if(value>bestScore){
            bestScore=value;
        }
    }
    void resetBestScore()
    {
        for(auto &t : sessions){
            t->resetBestScore();
        }
    }
    void playWholeRun()
    {
        for(auto &t : sessions){
            t->playWholeRun();
        }

        // update score
        setScore(getScore());
    }
    void mutate()
    {
        if(sessions.size()>0){
            // mutate first element
            sessions.first()->mutate();

            // copy same mutation to all other elements
            for(int i=1;i<sessions.size();i++){
                sessions[i]->copyBrain(sessions.first());
            }
        }
    }
    unsigned long long hash() const
    {
        return sessions.size() == 0 ? 0 : sessions.first()->hash();
    }
    double getBestPossibleScore() const
    {
        return sessions.size() == 0 ? 0 : sessions.first()->getBestPossibleScore();
    }

    QVector<T *> getSessions() const
    {
        return sessions;
    }
    void setSessions(const QVector<T *> &value)
    {
        sessions = value;
    }
    T *getAI()const
    {
        return sessions.first();
    }

    void draw(QPainter *p, int width, int height){
        const  int minimumSize=300;
        const int itemsPerRow=width/minimumSize;

        for(int i=0;i<sessions.size() && (i/itemsPerRow)*minimumSize + minimumSize <= height ;i++){
            p->save();
            T *t = sessions[i];
            int x = (i%itemsPerRow)*minimumSize;
            int y = (i/itemsPerRow)*minimumSize;
            p->translate(x, y);
            t->draw(p, minimumSize, minimumSize);
            p->restore();
        }
    }
    void step(){
        for(auto *t : sessions){
            t->step();
        }
    }
    void reset(){
        for(auto *t : sessions){
            t->reset();
        }
    }

private:
    QVector<T*> sessions;

};

#endif // MULTIGAMESEEKER_H
