#ifndef EYE_H
#define EYE_H

#include "QVector"

class Entity;
class MovingEntity;

class Eye
{
public:
    Eye(MovingEntity *owner, double angle);
    void drawItself();
    void step(QVector<Entity *> entities);
    double getAngle() const;
    double getDistance() const;
    double getInputValue()const;

    double getMaxDistance()const;

private:
    MovingEntity *owner;
    double angle;
    double distance = 0;
};

#endif // EYE_H
