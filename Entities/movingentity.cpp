#include "movingentity.h"
#include "Game/gamemanager.h"
#include "math.h"
#include <Qt>
#include <QtMath>
#include <utils/bigtimer.h>

const int MovingEntity::numberOfEyes = 8 ;

MovingEntity::MovingEntity(Qt::GlobalColor c, float x, float y):
    Entity(10, 10, x, y, c),
    gameManager(nullptr),
    fieldOfView(nullptr)
{
    initEyes();
}

MovingEntity::MovingEntity(const MovingEntity &m):
    Entity(m),
    gameManager(nullptr), // only pointer copy
    fieldOfView(nullptr)
{
    initEyes();
}

MovingEntity::~MovingEntity() {
    delete fieldOfView;

    qDeleteAll(eyes);
    eyes.clear();
}

void MovingEntity::stepItself(QVector<MovingEntity *> adversaries)
{
    BigTimer *t  = BigTimer::getInstance();
    t->start();
    Q_ASSERT_X(fieldOfView != nullptr && gameManager != nullptr, "", "Trying to step an uninitialised MovingEntity");

    checkCollisions();
    Coordinates next(this->getNextCoordinates());
    QPointF offset = QPointF(next.getX() - coordinates.getX(), next.getY() - coordinates.getY());
    coordinates = next;
    polygon.translate(offset);


    fieldOfView->stepItself(adversaries);


    for(auto eye : eyes){
        eye->step(gameManager->getEntities(this));
    }
    t->stop();
}

void MovingEntity::drawItself(QPainter *painter) {
    Q_ASSERT_X(fieldOfView != nullptr && gameManager != nullptr, "", "Trying to draw an uninitialised MovingEntity");
    // reset painter
    painter->save();
    // center painter current polygon center point
    const auto& coords = this->getCenterCoordinates();

    painter->translate(coords.getX(), coords.getY());
    // apply rotation
    painter->rotate(this->currentDirection*(180.0/M_PI));

    painter->setPen(color);
    painter->setBrush(color);

    // draw rectangle
    painter->fillRect(-this->width/2, -this->height/2, this->width, this->height, color);

    // draw eyes

    QColor rayColor = QColor(255, 255, 255, 40);
    rayColor.setAlpha(50);

    painter->setPen(rayColor);


    for (auto eye : eyes) {
        float x = cos(eye->getAngle()) * eye->getDistance();
        float y = sin(eye->getAngle()) * eye->getDistance();
        painter->drawLine(QLineF({0,0}, {x,y}));
    }


    // restore painter state^
    painter->restore();

    // draw field of view
    painter->setPen(fieldOfView->getColor());
    painter->setBrush(fieldOfView->getColor());
    painter->drawPolygon(fieldOfView->getPolygon());


    /*

    auto c(getCenterCoordinates());
    const auto &names = getInputsNames();
    const auto &inputs = makeInputs();
    painter->setPen(Qt::white);
    for(int i=0;i<names.size();i++){
        painter->drawText(c.getX(), c.getY()+i*10, names[i] + " " + QString::number(inputs[i], 'g', 2));
    }
    */

}

void MovingEntity::go() {
    velocity = 5;
}

Entity::Coordinates MovingEntity::getNextCoordinates() {
    Coordinates nextCoordinates;
    nextCoordinates.setX(coordinates.getX() + cos(currentDirection) * velocity);
    nextCoordinates.setY(coordinates.getY() + sin(currentDirection) * velocity);
    return nextCoordinates;
}

void MovingEntity::stand() {
    velocity = 0;
}

bool MovingEntity::isAboutToCollideWith(Entity *entity) {
    if (entity == this) {
        return false;
    }
    else {
        QPointF offset = QPointF(getNextCoordinates().getX() - coordinates.getX(), getNextCoordinates().getY() - coordinates.getY());
        QPolygonF futurPolygon = QPolygonF(polygon);
        futurPolygon.translate(offset);

        return futurPolygon.intersects(entity->getPolygon());
    }
}

float MovingEntity::getCurrentDirection() const
{
    return currentDirection;
}



void MovingEntity::turn(float degrees){
    currentDirection = fmod(currentDirection + (degrees * (M_PI/180.0)), 2*M_PI);
}

QVector<float> MovingEntity::getOtherPlayerDistances() const
{
    return otherPlayerDistances;
}


bool MovingEntity::isDetectingAdversary() const
{
    return detectingAdversary;
}

void MovingEntity::checkCollisions()
{
    for (auto x : gameManager->getEntities()) {
        if (isAboutToCollideWith(x)) {
            stand();
            break;
        }
    }
}

void MovingEntity::initEyes()
{
    for (float i = 0 ; i < numberOfEyes ; i++) {
        double angle = (i / double(numberOfEyes)) * (2 * M_PI);
        eyes.push_back(new Eye(this, angle));
    }
}

void MovingEntity::setCoordinates(const Entity::Coordinates &value)
{
    Entity::setCoordinates(value);
    fieldOfView->updatePolygon();
}

void MovingEntity::setGameManager(GameManager *value)
{
    gameManager = value;

    if(fieldOfView != nullptr)
        delete fieldOfView;

    createFieldOfView();
}

void MovingEntity::initInputs(int nAdversaries)
{
    otherPlayerDistances.clear();
    otherPlayerAngles.clear();
    for (int i = 0; i < nAdversaries ; i++)  {
        otherPlayerDistances.push_back(0);
        otherPlayerAngles.push_back(0);
    }
}

GameManager *MovingEntity::getGameManager() const
{
    return gameManager;
}

void MovingEntity::createFieldOfView()
{
    fieldOfView = new FieldOfView(gameManager, this);
}

QVector<double> MovingEntity::makeInputs() const
{
    QVector<double> res;
    for(auto x : otherPlayerDistances){
        res.append(double(x));
    }
    for(auto x : otherPlayerAngles){
        res.append(double(x));
    }
    for(auto x : eyes){
        res.append(double(x->getInputValue()));
    }
    return res;
}

QVector<QString> MovingEntity::getInputsNames() const
{
    QVector<QString> res;
    for(int i=0;i<otherPlayerDistances.size();i++){
        res.append("Dist(" + QString('A'+i) + ")");
    }
    for(int i=0;i<otherPlayerAngles.size();i++){
        res.append("Angl(" + QString('A'+i) + ")");
    }

    for(int i=0;i<eyes.size();i++){
        res.append("Sens(" + QString('A'+i) +")");
    }
    return res;
}

void MovingEntity::setOtherPlayerAngles(const QVector<float> &value)
{
    otherPlayerAngles = value;
}

void MovingEntity::setOtherPlayerDistances(const QVector<float> &value)
{
    otherPlayerDistances = value;
}

void MovingEntity::setDetectingAdversary(bool value)
{
    detectingAdversary = value;
}

float MovingEntity::getRelativeAngle(const Entity &other) const
{
    const MovingEntity::Coordinates myCoords(this->getCoordinates());
    const MovingEntity::Coordinates otherCoords(other.getCoordinates());

    float a1 = atan2(otherCoords.getY() - myCoords.getY(), otherCoords.getX() - myCoords.getX());
    float a2 = this->getCurrentDirection();
    float sign = a1 > a2 ? 1 : -1;
    float angle = a1 - a2;
    float K = -sign * M_PI * 2;
    angle = (abs(K + angle) < abs(angle))? K + angle : angle;

    return angle;
}

QVector<float> MovingEntity::getOtherPlayerAngles() const
{
    return otherPlayerAngles;
}

void MovingEntity::reset()
{
    this->currentDirection=0;
    coordinates.setX(0);
    coordinates.setY(0);
    standingTime=0;
    velocity=0;
}

int MovingEntity::getVelocity() const
{
    return velocity;
}

float MovingEntity::getStandingTime() const
{
    return standingTime;
}

void MovingEntity::setStandingTime(float value)
{
    standingTime = value;
}

