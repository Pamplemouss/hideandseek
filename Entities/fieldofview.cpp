#include "fieldofview.h"
#include "Game/gamemanager.h"
#include "movingentity.h"
#include <math.h>

FieldOfView::FieldOfView(GameManager *gameManager, MovingEntity *owner, int viewDistance, int angle, int rayCount) :
    gameManager(gameManager),
    owner(owner),
    viewDistance(viewDistance),
    fovAngle(angle*(M_PI/180.0)),
    rayCount(rayCount)
{
    updatePolygon();
}

FieldOfView::~FieldOfView() {
}

QPolygonF FieldOfView::getPolygon() const
{
    return polygon;
}

void FieldOfView::setPolygon(const QPolygonF &value)
{
    polygon = value;
}

void FieldOfView::stepItself(QVector<MovingEntity*> adversaries)
{
    updatePolygon();
    updateDetection(adversaries);
}

int FieldOfView::getViewDistance() const
{
    return viewDistance;
}

float FieldOfView::getFovAngle() const
{
    return fovAngle;
}

void FieldOfView::updatePolygon() {

    int x = owner->getCoordinates().getX() + owner->getWidth()/2;
    int y = owner->getCoordinates().getY() + owner->getHeight()/2;

    QPoint origin({x,y});
    QVector<QPointF> points;
    float angleIncrease = fovAngle / rayCount;
    float angle = 0;

    angle += -(fovAngle/2); // Offset l'angle pour que la ligne de vue soit centrée et dirigée vers le bas
    angle += (owner->getCurrentDirection()); // Rotate le FoV dans la direction du possesseur

    points.push_back(origin);

    for (int i = 0; i <= rayCount; i++) {
        QPointF newPoint = QPoint(origin.x() + cos(angle) * viewDistance, origin.y() + sin(angle) * viewDistance);
        for(auto *wall : gameManager->getWalls()){

            QLineF ray = wall->rayCollision(QLineF(origin, newPoint));
            if(!ray.isNull()){
                newPoint = ray.p2();
            }

        }
        points.push_back(newPoint);
        angle += angleIncrease;
    }
    polygon = points;
}

void FieldOfView::updateDetection(QVector<MovingEntity*> adversaries) {
    bool isDetectingAdversary = false;
    QVector<float> distances = owner->getOtherPlayerDistances();
    QVector<float> angles = owner->getOtherPlayerAngles();

    for (int i = 0; i < adversaries.length() ; i++)  {
        if (polygon.intersects(adversaries[i]->getPolygon())) {
            isDetectingAdversary = true;
            distances[i] = fmax(0, fmin(1 - ((owner->getDistance(*adversaries[i]) - owner->getWidth() - 1) / viewDistance), 1)); // On normalise pour avoir une valeur entre 0 et 1 && On retire width et -1 pour l'épaisseur du personnage
            angles[i] = (owner->getRelativeAngle(*adversaries[i]) / (fovAngle/2.0) + 1) / 2.0; // On normalise pour avoir une valeur entre 0 et 1

        }
        else {
            distances[i] = 0;
            angles[i] = 0;
        }
    }

    owner->setOtherPlayerDistances(distances);
    owner->setOtherPlayerAngles(angles);

    owner->setDetectingAdversary(isDetectingAdversary);
    if(owner->isDetectingAdversary()) color = (QColor(255,0,0,100));
    else color = QColor(135, 211, 124, 127);
}

QColor FieldOfView::getColor() const
{
    return color;
}

void FieldOfView::setColor(const QColor &value)
{
    color = value;
}
