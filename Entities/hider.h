#ifndef HIDER_H
#define HIDER_H
#include "movingentity.h"

class GameManager;

class Hider: public MovingEntity
{
public:
    Hider(float x=0, float y=0);
    Hider(const Hider& h);
    void createFieldOfView();
    virtual ~Hider();
    virtual void stepItself();
};

#endif // HIDER_H
