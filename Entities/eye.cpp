#include "eye.h"
#include "entity.h"
#include "movingentity.h"
#include "math.h"
#include <QtGlobal>

Eye::Eye(MovingEntity *owner, double angle): angle(angle), owner(owner)
{

}

void Eye::drawItself()
{

}

void Eye::step(QVector<Entity *> entities)
{
    bool detecting = false;
    const int infiniteDistance = 10000;
    distance =  infiniteDistance;

    // create a ray from our center position to an infinite distance
    QLineF ray(owner->getCenterCoordinates().getX(), owner->getCenterCoordinates().getY(),
              cos(angle + owner->getCurrentDirection()) * infiniteDistance + owner->getCenterCoordinates().getX(), sin(angle + owner->getCurrentDirection()) * infiniteDistance + owner->getCenterCoordinates().getY());

    for(auto *e: entities){
        QLineF newRay(e->rayCollision(ray));
          if(!newRay.isNull() && newRay.length() < distance){
            distance = qMin(newRay.length(), getMaxDistance());
            detecting = true;
          }
    }

    Q_ASSERT_X(detecting, "void Eye::update()", "Eye didn't detect anything !");
}

double Eye::getDistance() const
{
    return distance;
}

double Eye::getInputValue() const
{
    return 1.0  - distance/getMaxDistance();
}

double Eye::getMaxDistance() const
{
    return 300;
}

double Eye::getAngle() const
{
    return angle;
}
