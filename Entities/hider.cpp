#include "hider.h"
#include "seeker.h"
#include "Game/gamemanager.h"
#include <math.h>
#include <algorithm>

Hider::Hider(float x, float y) : MovingEntity(Qt::blue, x, y) {

}

Hider::Hider(const Hider &h):
    MovingEntity(h)
{

}

void Hider::createFieldOfView()
{
    fieldOfView = new FieldOfView(gameManager, this, 100, 270, 40);
}

Hider::~Hider()
{

}

void Hider::stepItself() {
    QVector<MovingEntity *> adversaries;
    for (int i = 0; i < gameManager->getSeekers().length() ; i++) {
        adversaries.push_back(gameManager->getSeekers()[i]);
    }

    MovingEntity::stepItself(adversaries);
}

