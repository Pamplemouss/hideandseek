#include "entity.h"
#include <math.h>
#include <Collision/collisiondetector.h>

Entity::Entity(int width, int height, float x, float y, Qt::GlobalColor color)
    : height(height),
      width(width),
      coordinates(x,y),
      color(color)
{
    polygon = QRectF(x, y, width, height);
}

Entity::Coordinates::Coordinates(float x, float y) : x(x), y(y) {
}

void Entity::Coordinates::setX(float value) {
    x = value;
}

void Entity::Coordinates::setY(float value) {
    y = value;
}

float Entity::Coordinates::getX() const {
    return x;
}

float Entity::Coordinates::getY() const {
    return y;
}

void Entity::drawItself(QPainter *painter)
{
    painter->setPen(color);
    painter->setBrush(color);
    painter->drawPolygon(polygon);
}


Entity::Coordinates Entity::getCoordinates() const {
    return coordinates;
}

Entity::Coordinates Entity::getCenterCoordinates() const
{
    return Coordinates(coordinates.getX() + width/2, coordinates.getY() + height/2);
}

int Entity::getHeight()const {
    return height;
}

int Entity::getWidth()const {
    return width;
}

Qt::GlobalColor Entity::getColor() const
{
    return color;
}

void Entity::setColor(const Qt::GlobalColor &value)
{
    color = value;
}

void Entity::setCoordinates(const Coordinates &value)
{
    coordinates = value;
    polygon = QRectF(coordinates.getX(), coordinates.getY(), width, height);
}

bool Entity::collides(const QLineF &line) const
{
    return CollisionDetector::lineCollision(line, polygon.boundingRect());
}

QLineF Entity::rayCollision(const QLineF &line)
{
    QLineF res;    // return an invalid line if no collision

    // ensure at least one line intersects as it is faster
    if(this->collides(line)){
        QRectF bounds(polygon.boundingRect());
        // check collision on all bounding lines

        QVector<QLineF> boundingsLines({QLineF(bounds.topLeft(), bounds.topRight()),
                                       QLineF(bounds.topRight(), bounds.bottomRight()),
                                       QLineF(bounds.bottomRight(), bounds.bottomLeft()),
                                       QLineF(bounds.bottomLeft(), bounds.topLeft())});

        /*
         * More precise but slow method
        QVector<QLineF> boundingsLines(polygon.size());
        for(int i=0;i<polygon.size();i++){
            int previous = (i == 0 ? polygon.size()-1 : i-1);
            int current = i;
            QLineF boundingLine(polygon[previous], polygon[current]);
                boundingsLines.append(boundingLine);

        }
        */

        for(const auto &bLine : boundingsLines){
            QLineF ray;
            QPointF intersectionPoint;
            QLineF::IntersectType intersectionType(line.intersects(bLine, &intersectionPoint));
            if(intersectionType == QLineF::BoundedIntersection){
                ray.setP1(line.p1());
                ray.setP2(intersectionPoint);
                // if ray is smaller than previous ray
                if(res.isNull() || res.length() > ray.length()){
                    res = ray;
                }
            }
        }
    }

    return res;
}

bool Entity::collides(const Entity &other) const
{
    return this->polygon.intersects(other.polygon);
}

double Entity::getDistance(const Entity &other) const
{
    auto myCoords = this->getCenterCoordinates();
    auto otherCoords = other.getCenterCoordinates();

    float x1 = myCoords.getX();
    float y1 = myCoords.getY();
    float x2 = otherCoords.getX();
    float y2 = otherCoords.getY();

    // calculating distance formula : √[(x₂ - x₁)² + (y₂ - y₁)²]
    return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));

}


QPolygonF Entity::getPolygon() const
{
    return polygon;
}

