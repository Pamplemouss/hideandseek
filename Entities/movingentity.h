#ifndef MOVINGENTITY_H
#define MOVINGENTITY_H

#include "entity.h"
#include "fieldofview.h"
#include "eye.h"

class GameManager;

class MovingEntity: public Entity
{
public:
    MovingEntity(Qt::GlobalColor color, float x=0, float y=0);
    MovingEntity(const MovingEntity &m);
    virtual ~MovingEntity();
    virtual void stepItself(QVector<MovingEntity *> adversaries);
    virtual void stepItself()=0;

    virtual void drawItself(QPainter *painter);
    void go();
    void stand();
    void turn(float degrees);
    bool isAboutToCollideWith(Entity *entity);

    Coordinates getNextCoordinates();
    float getCurrentDirection() const;


    void setGameManager(GameManager *value);
    virtual void initInputs(int nAdversaries);
    GameManager *getGameManager() const;
    virtual void createFieldOfView();

    QVector<double> makeInputs()const;
    void reset();

    int getVelocity() const;

    float getStandingTime() const;
    void setStandingTime(float value);
    QVector<QString> getInputsNames()const;

    QVector<float> getOtherPlayerAngles() const;
    QVector<float> getOtherPlayerDistances() const;
    void setOtherPlayerAngles(const QVector<float> &value);
    void setOtherPlayerDistances(const QVector<float> &value);
    bool isDetectingAdversary() const;
    void setDetectingAdversary(bool value);

    float getRelativeAngle(const Entity &other) const;
    void setCoordinates(const Coordinates &value);

    static const int numberOfEyes;

protected:
    GameManager *gameManager;
    FieldOfView *fieldOfView;
    float currentDirection = 0; // En radian
    int velocity = 0;

    float standingTime=0;
    QVector<float> otherPlayerDistances; // 1 si collé, 0 si au max du champs de vision
    QVector<float> otherPlayerAngles; // -1 si full gauche, 0 centre, 1 full droite (relatif à la direction du personnage)
    QVector<Eye*> eyes;
    bool detectingAdversary = false;
    void checkCollisions();
    void initEyes();


};

#endif // MOVINGENTITY_H
