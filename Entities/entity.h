#ifndef ENTITY_H
#define ENTITY_H


#include <QPainter>

class Entity
{
public:
    class Coordinates {
    public:
        Coordinates(float x=0, float y=0);
        float getX() const;
        void setX(float value);

        float getY() const;
        void setY(float value);

    private:
        float x;
        float y;
    };

    Entity(int width=10, int height=10, float x=0, float y=0, Qt::GlobalColor color=Qt::red);
    virtual ~Entity(){}
    virtual void drawItself(QPainter *painter);
    Coordinates getCoordinates()const;
    Coordinates getCenterCoordinates()const;

    QPolygonF getPolygon() const;
    int getHeight()const;
    int getWidth() const;
    Qt::GlobalColor getColor() const;
    void setColor(const Qt::GlobalColor &value);
    virtual void setCoordinates(const Coordinates &value);

    bool collides(const QLineF &line)const;
    QLineF rayCollision(const QLineF& ray);
    bool collides(const Entity &other)const;
    double getDistance(const Entity &other)const;


protected:
    QPolygonF polygon;
    Qt::GlobalColor color;
    int height = 10;
    int width = 10;
    Coordinates coordinates;
};

#endif // ENTITY_H
