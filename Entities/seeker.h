#ifndef SEEKER_H
#define SEEKER_H
#include "movingentity.h"

class GameManager;

class Seeker: public MovingEntity
{
public:
    Seeker(float x=0, float y=0);
    Seeker(const Seeker& s);
    virtual void stepItself();



    // MovingEntity interface
public:
    void createFieldOfView();
};

#endif // SEEKER_H
