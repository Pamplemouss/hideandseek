#include "seeker.h"
#include "hider.h"
#include "Game/gamemanager.h"
#include <math.h>
#include <algorithm>

Seeker::Seeker(float x, float y) : MovingEntity(Qt::red, x, y) {

}

Seeker::Seeker(const Seeker &s):
    MovingEntity(s)
{

}


void Seeker::stepItself() {
    QVector<MovingEntity *> adversaries;
    for (int i = 0; i < gameManager->getHiders().length() ; i++) {
        adversaries.push_back(gameManager->getHiders()[i]);
    }

    MovingEntity::stepItself(adversaries);
}

void Seeker::createFieldOfView()
{
    fieldOfView = new FieldOfView(gameManager, this, 100, 120, 20);
}

