#ifndef FIELDOFVIEW_H
#define FIELDOFVIEW_H

#include <QPainter>

class GameManager;
class MovingEntity;

class FieldOfView
{
public:
    FieldOfView(GameManager *gameManager, MovingEntity *owner, int viewDistance = 100, int angle = 90, int rayCount=5);
    ~FieldOfView();

    QPolygonF getPolygon() const;
    QColor getColor() const;
    void setColor(const QColor &value);
    void setPolygon(const QPolygonF &value);
    void stepItself(QVector<MovingEntity*> adversaries);
    int getViewDistance() const;
    float getFovAngle() const;
    void updatePolygon();

private:
    GameManager *gameManager;
    MovingEntity *owner;
    QPolygonF polygon;
    QColor color = QColor(135, 211, 124, 127);
    int viewDistance;
    float fovAngle;
    int rayCount;

    void updateDetection(QVector<MovingEntity*> adversaries);
};

#endif // FIELDOFVIEW_H
