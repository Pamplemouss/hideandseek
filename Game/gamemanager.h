#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include "Entities/entity.h"
#include "Entities/seeker.h"
#include "Entities/hider.h"

#include "room.h"

#include <Training/abstractgamemanager.h>

class GameManager: public AbstractGameManager
{
public:
    enum PLAYERTYPE{NONE, SEEKER, HIDER};

    GameManager();
    GameManager(const GameManager& m);
    virtual ~GameManager();
    QVector<Entity *> getWalls() const;
    QVector<Hider *> getHiders() const;
    QVector<Seeker *> getSeekers() const;
    void step();
    void draw(QPainter *painter)const;
    bool isGameOver();
    void restart();
    PLAYERTYPE getWinner() const;


    void setSeekers(const QVector<MovingEntity *> &value);
    void setHiders(const QVector<MovingEntity *> &value);

    void replacePlayers();
    void updateScores();

    QVector<Entity *> getEntities();
    QVector<Entity *> getEntities(Entity *exception);

private:
    QVector<Seeker *> seekers;
    QVector<Hider *> hiders;

    int stepCount = 0;
    int maxStepCount;
    double seekerFreezeTime;
    const double winDetectionRatio;
    float detectionScore = 0;
    PLAYERTYPE winner = NONE;
    Room *room;

};

#endif // GAMEMANAGER_H
