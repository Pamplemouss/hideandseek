#ifndef FLOORTILE_H
#define FLOORTILE_H

#include <QPainter>

#include "room.h"

class FloorTile
{
public:
    FloorTile(int x, int y, int size);
    void drawItself(QPainter *p, Room::DISPLAY displayMode)const;
    void reset();
    void addSeekerStep();
    void addHiderStep();
    void fadeFootPrint(double ratio);

private:
    int x;
    int y;
    int size;
    int seekerSteps;
    int hiderSteps;
};

#endif // FLOORTILE_H
