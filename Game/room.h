#ifndef ROOM_H
#define ROOM_H

#include <utils/random.h>
#include "Entities/entity.h"

#include <QMutex>

#include <QPainter>
#include <Entities/hider.h>
#include <Entities/seeker.h>

class FloorTile;

class Room
{
public:
    enum DISPLAY{NORMAL, LIGHT};

    Room(int width, int height, int seed=0);
    Room(const Room& r);
    ~Room();
    void reset();
    void reset(int seed);
    void draw(QPainter *p, DISPLAY displayMode=NORMAL);

    void notifyStep(const Seeker &entity);
    void notifyStep(const Hider &entity);

    void fadeFootPrint(double ratio);

    static Room* getInstance();
    static Room *getCombinedFootPrintInstance();

    QVector<Entity *> getWalls() const;

private:
    void createRoom();
    void deleteRoom();

    void createVerticalDoor();
    void createHorizontalDoor();

    int width;
    int height;
    int seed;
    const int wallWidth = 10;
    const int floorTileWidth = wallWidth;
    Random r;

    QMutex mutex;

    QVector<Entity*> walls;
    QVector<FloorTile> tiles;

    static Room *mainInstance;
    static Room *combinedFootPrintInstance;

};

#endif // ROOM_H
