#include "floortile.h"
#include "utils/utils.h"
#include <QtMath>
FloorTile::FloorTile(int x, int y, int size):
    x(x),
    y(y),
    size(size)
{
    reset();
}

void FloorTile::drawItself(QPainter *p, Room::DISPLAY displayMode) const
{
    QColor color;
    int seekerColorWeight=0;
    int hiderColorWeight=0;
    if(displayMode == Room::NORMAL){

        seekerColorWeight = seekerSteps==0 ?  0 : seekerSteps*20 + 50;
        hiderColorWeight = hiderSteps == 0 ? 0 : hiderSteps*20 + 50;
    }
    else{
        if(seekerSteps>0){
            seekerColorWeight = seekerSteps*7+40;
        }
        if(hiderSteps>0){
            hiderColorWeight = hiderSteps*7+40;
        }
    }
    p->fillRect(x,y, size, size, Utils::blendColors(QColor(qMin(seekerColorWeight, 255), 0, 0), QColor(0,0,qMin(hiderColorWeight, 255))));
}

void FloorTile::reset()
{
    seekerSteps=0;
    hiderSteps=0;
}

void FloorTile::addSeekerStep()
{
    seekerSteps++;
}

void FloorTile::addHiderStep()
{
    hiderSteps++;
}

void FloorTile::fadeFootPrint(double ratio)
{
    seekerSteps=qFloor(seekerSteps*ratio);
    hiderSteps=qFloor(hiderSteps*ratio);
}
