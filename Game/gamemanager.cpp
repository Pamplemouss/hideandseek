#include "Game/gamemanager.h"
#include <math.h>
#include <AI/hiderai.h>
#include <AI/seekerai.h>

GameManager::GameManager():
    maxStepCount(400),
    seekerFreezeTime(0.2),
    room(new Room(*Room::getInstance())),
    winDetectionRatio(0.5)
{

}

GameManager::GameManager(const GameManager &m):
    maxStepCount(m.maxStepCount),
    seekerFreezeTime(m.seekerFreezeTime),
    seekers(m.seekers), // only copy pointers and do not create seperate objects (infinite recursive loops otherwise)
    hiders(m.hiders),
    room(new Room(*Room::getInstance())),
    winDetectionRatio(m.winDetectionRatio)
{
}

GameManager::~GameManager() {
    delete room;
}

QVector<Entity *> GameManager::getWalls() const
{
    return room->getWalls();
}


void GameManager::step() {
    if(winner==NONE){
        stepCount++;

        //for each hider
        for (int i = 0; i < hiders.length(); i++) {
            hiders[i]->stepItself();
            if(hiders[i]->getVelocity()==0){
                hiders[i]->setStandingTime(hiders[i]->getStandingTime()+1);
            }

            room->notifyStep(*hiders[i]);
        }

        bool seekersDetecting = false;

        // freeze seekers at the begining of game
        if(stepCount>maxStepCount*seekerFreezeTime){
            //for each seeker
            for (int i = 0; i < seekers.length(); i++) {
                seekers[i]->stepItself();
                if(seekers[i]->getVelocity()==0){
                    seekers[i]->setStandingTime(seekers[i]->getStandingTime()+1);
                }
                room->notifyStep(*seekers[i]);
            }

            for (int i = 0; i < seekers.length(); i++) {
                if (seekers[i]->isDetectingAdversary()) seekersDetecting = true;
            }
        }


        if (seekersDetecting) {
            float closestHiderDistanceScore = 0;
            for (int i = 0; i < seekers.length(); i++) {
                for (int j = 0 ; j < seekers[i]->getOtherPlayerDistances().length() ; j++) {
                    float distanceFromHiderScore = seekers[i]->getOtherPlayerDistances()[j];

                    if (distanceFromHiderScore > closestHiderDistanceScore) closestHiderDistanceScore = distanceFromHiderScore;
                }
            }
            //detectionScore += closestHiderDistanceScore;
            detectionScore += 100.0/(double(maxStepCount * (1.0 - seekerFreezeTime))) * 1.0/winDetectionRatio;
        }
        //else detectionScore = fmax(detectionScore-1, 0);

        //updateScores();
    }
}

void GameManager::draw(QPainter *painter)const {
    int scores=1;

    room->draw(painter);

    for (int i = 0; i < seekers.length(); i++) {
        seekers[i]->drawItself(painter);
        painter->setPen(Qt::gray);
        painter->drawText(60, scores++*10 + 20, "Seeker score: " + QString::number((dynamic_cast<HideNSeekAI*>(seekers[i]))->getScore()));
    }
    for (int i = 0; i < hiders.length(); i++) {
        hiders[i]->drawItself(painter);
        painter->setPen(Qt::gray);
        painter->drawText(60, scores++*10 + 20, "Hider score: " + QString::number((dynamic_cast<HideNSeekAI*>(hiders[i]))->getScore()));
    }
}

bool GameManager::isGameOver(){
    if (stepCount >= maxStepCount || detectionScore>=100.0) {
        if(detectionScore>=50.0){
            winner = SEEKER;
        }
        else{
            winner = HIDER;
        }
        updateScores();
        return true;
    }

    else return false;
}

void GameManager::restart()
{
    //qDeleteAll(seekers.begin(), seekers.end());
    //qDeleteAll(hiders.begin(), hiders.end());
    room->reset();

    stepCount = 0;
    detectionScore = 0;
    winner = NONE;

    replacePlayers();
    // notify number of inputs (since it depends on the number of opponents) TODO: make it static (ex: max 3 detectable enemies)
    // Le +20 c'est pour le nombre de yeux
    int nInputs = hiders.size() * 2 + MovingEntity::numberOfEyes;
    for(auto *s : seekers){
        SeekerAI  *sai = dynamic_cast<SeekerAI*>(s);
        sai->initInputs(hiders.size());
    }
    nInputs = seekers.size() * 2 + MovingEntity::numberOfEyes;
    for(auto *h : hiders){
        HiderAI  *hai = dynamic_cast<HiderAI*>(h);
        hai->initInputs(seekers.size());
    }
}

GameManager::PLAYERTYPE GameManager::getWinner() const
{
    return winner;
}

void GameManager::setSeekers(const QVector<MovingEntity *> &value)
{
    seekers.clear();
    for(auto *v : value){
        seekers.append(dynamic_cast<Seeker*>(v));
    }

    for(auto* s : seekers){
        s->setGameManager(this);
    }
}

void GameManager::setHiders(const QVector<MovingEntity *> &value)
{
    hiders.clear();
    for(auto *v : value){
        hiders.append(dynamic_cast<Hider*>(v));
    }
    for(auto* h : hiders){
        h->setGameManager(this);
    }
}

void GameManager::replacePlayers()
{
    for(int i=0;i<seekers.size();i++){
        seekers[i]->reset();
        seekers[i]->setCoordinates(Entity::Coordinates(50+i*50,50));
    }

    for(int i=0;i<hiders.size();i++){
        hiders[i]->reset();
        hiders[i]->setCoordinates(Entity::Coordinates(200+i*50,200));
    }
}

void GameManager::updateScores()
{
    double hidersScore = 100.0 - detectionScore;
    double seekersScore= detectionScore;
    for(MovingEntity *s : seekers){
        SeekerAI *seeker = dynamic_cast<SeekerAI*>(s);
        // add a penalty for seekers who just stand facing a wall
        seeker->setScore(seekersScore );
    }

    for(MovingEntity *h : hiders){
        HiderAI *hider = dynamic_cast<HiderAI*>(h);
        hider->setScore(hidersScore);
    }
}

QVector<Entity *> GameManager::getEntities()
{
    QVector<Entity *> entities;

    for(auto x : seekers) {
        entities.push_back(x);
    }
    for(auto x : hiders) {
        entities.push_back(x);
    }
    for(auto x : room->getWalls()) {
        entities.push_back(x);
    }

    return entities;
}

QVector<Entity *> GameManager::getEntities(Entity *exception)
{
    QVector<Entity *> entities;

    for(auto x : seekers) {
        entities.push_back(x);
    }
    for(auto x : hiders) {
        entities.push_back(x);
    }
    for(auto x : room->getWalls()) {
        entities.push_back(x);
    }

    entities.removeOne(exception);

    return entities;
}


QVector<Hider *> GameManager::getHiders() const
{
    return hiders;
}

QVector<Seeker *> GameManager::getSeekers() const
{
    return seekers;
}

