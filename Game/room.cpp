#include "room.h"
#include "floortile.h"
#include <Entities/entity.h>
#include <QMutexLocker>

Room *Room::mainInstance=nullptr;
Room *Room::combinedFootPrintInstance=nullptr;

Room::Room(int width, int height, int seed):
    width(width),
    height(height),
    seed(seed)
{
    for(int y=0;y<height;y+=floorTileWidth){
        for(int x=0;x<width;x+=floorTileWidth){
            tiles.append(FloorTile(x, y, floorTileWidth));
        }
    }

    reset(seed);
}

Room::Room(const Room &r):
    width(r.width),
    height(r.height),
    seed(r.seed)
{
    const int floorTileWidth = wallWidth;
    for(int y=0;y<height;y+=floorTileWidth){
        for(int x=0;x<width;x+=floorTileWidth){
            tiles.append(FloorTile(x, y, floorTileWidth));
        }
    }

    reset(seed);
}

Room::~Room()
{
    deleteRoom();
}

void Room::reset()
{
    reset(this->seed);
}

void Room::reset(int seed)
{
    QMutexLocker l(&mutex);

    deleteRoom();

    this->seed = seed;

    r.seed(seed);

    for(auto &t : tiles){
        t.reset();
    }
    createRoom();
}

void Room::draw(QPainter *p, DISPLAY displayMode)
{
    QMutexLocker l(&mutex);

    for(const auto &t : tiles){
        t.drawItself(p, displayMode);
    }

    for (auto *w : walls) {
        w->drawItself(p);
    }
}

void Room::notifyStep(const Hider &entity)
{
    int caseX = qRound(entity.getCoordinates().getX()/floorTileWidth);
    int caseY = qRound(entity.getCoordinates().getY()/floorTileWidth);
    int tilesPerLine = width/floorTileWidth;
    FloorTile &t = tiles[caseX + caseY*tilesPerLine];
    t.addHiderStep();

    if(this != getCombinedFootPrintInstance()){
        combinedFootPrintInstance->tiles[caseX + caseY*tilesPerLine].addHiderStep();
    }
}

void Room::fadeFootPrint(double ratio)
{
    for(auto &t : tiles){
        t.fadeFootPrint(ratio);
    }
}

void Room::notifyStep(const Seeker &entity)
{
    int caseX = qRound(entity.getCoordinates().getX()/floorTileWidth);
    int caseY = qRound(entity.getCoordinates().getY()/floorTileWidth);
    int tilesPerLine = width/floorTileWidth;
    FloorTile &t = tiles[caseX + caseY*tilesPerLine];
    t.addSeekerStep();

    if(this != getCombinedFootPrintInstance()){
        combinedFootPrintInstance->tiles[caseX + caseY*tilesPerLine].addSeekerStep();
    }
}

Room *Room::getInstance()
{
    if(mainInstance==nullptr){
        mainInstance = new Room(300, 300, Random().i(200000));
    }
    return mainInstance;
}

Room *Room::getCombinedFootPrintInstance()
{
    if(combinedFootPrintInstance==nullptr){
        combinedFootPrintInstance = new Room(*getInstance());
    }
    return combinedFootPrintInstance;
}

void Room::createRoom()
{


    int doors = 2;// r.i(2);
    bool topDoor = r.b();

    // surrounding walls
    walls.push_back(new Entity(wallWidth, height, 0, 0, Qt::gray));
    walls.push_back(new Entity(width, wallWidth, 0, 0, Qt::gray));
    walls.push_back(new Entity(wallWidth, height, width-wallWidth, 0, Qt::gray));
    walls.push_back(new Entity(width, wallWidth, 0, height-wallWidth, Qt::gray));

    //walls.push_back(new Entity(width/2-40, wallWidth, 40, (height/4)-(wallWidth/2), Qt::gray));


    // create both doors
    if(doors == 2){
        createHorizontalDoor();
        createVerticalDoor();
    }
    // only one door
    else{
        if(topDoor){
            // full vertical wall (no door on left)
            walls.push_back(new Entity(wallWidth, height/2, (width/2), (height/2), Qt::gray));

            createHorizontalDoor();
        }
        else{
            // full horizontal wall (no door on top)
            walls.push_back(new Entity(width/2, wallWidth, (width/2), (height/2), Qt::gray));

            // middle vertical, bottom
            createVerticalDoor();
        }
    }



}

void Room::deleteRoom()
{
    qDeleteAll(walls);
    walls.clear();
}

void Room::createVerticalDoor()
{
    int size = 35;
    int pos = r.i(height/2 - wallWidth*2 - size) + wallWidth;
    walls.push_back(new Entity(wallWidth, pos, (width/2), (height/2), Qt::gray));
    walls.push_back(new Entity(wallWidth, height/2 - pos -  size, (width/2), (height/2) + pos + size, Qt::gray));
}

void Room::createHorizontalDoor()
{
    int size =  35;
    int pos = r.i(width/2 - wallWidth*2 - size) + wallWidth;

    walls.push_back(new Entity(pos, wallWidth, (width/2), (height/2), Qt::gray));
    walls.push_back(new Entity(width/2 - pos -  size, wallWidth, (width/2) + pos + size, (height/2) , Qt::gray));
}

QVector<Entity *> Room::getWalls() const
{
    return walls;
}
