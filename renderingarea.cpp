#include "renderingarea.h"
#include <QPainter>
#include <QTimer>
#include <QKeyEvent>
#include "Game/gamemanager.h"
#include <QtDebug>
#include <QtGlobal>
#include <Qt>
#include <utils/bigtimer.h>
RenderingArea::RenderingArea(QWidget *parent): QOpenGLWidget(parent),
    trainingCyclesPerGroup(20),
    trainingCycles(0),
    hiderFactory(new HiderFactory()),
    seekerFactory(new SeekerFactory()),
    trainHiders(false),
    bestHiderScore(-1000),
    bestSeekerScore(-1000),
    totalTrainingCycles(0)
{
    const int poolSize = 10;
    const double keep = 0.25;
    render=true;

    painter = new QPainter;
    timer =  new QTimer;
    gameTick = new QTimer;
    connect(timer,SIGNAL(timeout()),this,SLOT(update()));
    timer->start(25);
    setFocusPolicy(Qt::FocusPolicy::StrongFocus);

    //  init factories
    bestSeeker = new SeekerAI("InitialSeeker");
    GameManager *g1(new GameManager());
    g1->setSeekers({bestSeeker});
    hiderFactory->setOpponents({bestSeeker}); // init opponents with a basic untrained one
    g1->restart();

    bestHider = new HiderAI("InitialHider");
    GameManager *g2(new GameManager());
    g2->setHiders({bestHider});
    seekerFactory->setOpponents({bestHider}); // init opponents with a basic untrained one
    g2->restart();

    // recreate base models for display
    bestHider = dynamic_cast<HiderAI*>(hiderFactory->create(*bestHider));
    bestSeeker = dynamic_cast<SeekerAI*>(seekerFactory->create(*bestSeeker));

    // init genetic trainers
    hidersTrainer = new GeneticTrainer(hiderFactory, poolSize, keep);
    hidersTrainer->setGenerationToKeep(0);
    seekersTrainer = new GeneticTrainer(seekerFactory, poolSize, keep);
    seekersTrainer->setGenerationToKeep(0);

    // init OpponentsTrainer using genetic trainers
    trainer = new OpponentsTrainer(hidersTrainer, seekersTrainer, 20);

    // training signals
    connect(trainer, &OpponentsTrainer::newBestScore, this, &RenderingArea::onNewBestScore);

    // score history connection
    connect(seekersTrainer, &GeneticTrainer::trainingDone, &scoreHistory, &ScoreHistory::updateSeeker);
    connect(hidersTrainer, &GeneticTrainer::trainingDone, &scoreHistory, &ScoreHistory::updateHider);

    // start trainer
    trainer->start();

    displayGame  = bestSeeker->getGameManager();

    // launch live game view
    connect(gameTick, SIGNAL(timeout()), this, SLOT(step()));
    gameTick->setSingleShot(true);
    gameTick->start();

}
RenderingArea::~RenderingArea(){
    trainer->exit();
    trainer->wait(10000);
    delete timer;
    delete painter;
}
void RenderingArea::initializeGL()
{
    QOpenGLWidget::initializeGL();
}

void RenderingArea::resizeGL(int w, int h)
{
    QOpenGLWidget::resizeGL(w, h);
}

void RenderingArea::paintGL()
{
   /*painter->begin(this);

   //if (gameManager.isGameOver()) gameManager.restart();
   gameManager.step();
   gameManager.draw(this->painter);

   painter->end();
   */
   QMutexLocker l(&mutex);

   //int offsetX = (this->width()-snakeGame->getBoardWidth()*snakeGame->getCaseSize())/2;
   //int offsetY = (this->height()-snakeGame->getBoardHeight()*snakeGame->getCaseSize())/2;

   //QTime currentFrameDuration;
   QElapsedTimer currentFrameDuration;
   currentFrameDuration.start();

   QVector<TrainingMetrics> metrics;
   metrics.append(hidersTrainer->getMetrics());
   metrics.append(seekersTrainer->getMetrics());

   painter->begin(this);

   if(render){

       painter->save();
       painter->translate(50, 50);
       displayGame->draw(painter);
       painter->restore();

       for(int i=0;i<metrics.size();i++){
           TrainingMetrics &metric = metrics[i];
           const int metricSize = 120;
           const int gamePreviewSize = 500;
           int gamePixelSize = metricSize + 5;

           int gamesPerRow = qMax((width()-gamePreviewSize)/gamePixelSize, 1);

           int gameX = gamePreviewSize + (i%gamesPerRow)*gamePixelSize;
           int gameY = (i/gamesPerRow)*gamePixelSize*2;
           metric.draw(painter, metricSize, gameX, gameY);
       }

       painter->save();
       painter->translate(50, 400);
       scoreHistory.draw(painter);
       painter->restore();


       if(displayGame->isGameOver()){
           restartGame();
       }
   }


   painter->end();
   timer->start(qMax(int(1000/60.0 - double(currentFrameDuration.elapsed())), 0));

}

void RenderingArea::restartGame()
{
    // start a new game using our best AIs
    displayGame->restart();
    gameTick->start();
}

void RenderingArea::step()
{

    //QMutexLocker l(&mutex);
    QElapsedTimer currentFrameDuration;
    currentFrameDuration.start();
    mutex.lock();

    if(render){
        displayGame->updateScores();
        displayGame->step();
    }

    gameTick->start(qMax(int(1000/40.0 - double(currentFrameDuration.elapsed())), 0));
    mutex.unlock();
}


void RenderingArea::onNewBestScore(Trainable *t1, Trainable *t2)
{
    QMutexLocker l(&mutex);
    seekerFactory->remove(bestSeeker); // removing seeker will also remove the hider as the whole game will be deleted

    // update best entities
    bestSeeker = dynamic_cast<SeekerAI*>(t2);
    bestHider = dynamic_cast<HiderAI*>(t1);

    // update display game
    bestSeeker->getGameManager()->setHiders({bestHider});
    displayGame = bestSeeker->getGameManager();


}
