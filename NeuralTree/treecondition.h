#ifndef TREECONDITION_H
#define TREECONDITION_H

#include "treeio.h"
#include "mutableelement.h"

#include <QPainter>
#include <QVector>


class TreeNode;

class TreeCondition: public MutableElement
{
public:
    enum TYPE{OR=0, A, AE, E};

    TreeCondition(QVector<TreeIO> *inputs,  QVector<TreeIO> *actions, TreeNode *parentNode, const TreeCondition& c);
    TreeCondition(QVector<TreeIO> *inputs,  QVector<TreeIO> *actions, TreeNode *parentNode, bool enableOrGate);
    void draw(QPainter *p)const;

    virtual ~TreeCondition();

    bool isTrue();

    void setOp(const TYPE &isTrue);

    void setV1(int offset);

    void setV2(int offset);
    unsigned long long hash() const;


private:
    QVector<TreeIO> *inputs;
    QVector<TreeIO> *actions;
    QVector<int> constantsId;

    int v1;
    int v2;
    TYPE op;
    bool reverse;
    TreeNode *parentNode;
    // MutableElement interface
public:
    void mutate(bool reverseCondition=false);
    void setInputs(QVector<TreeIO> *isTrue);
    void setActions(QVector<TreeIO> *isTrue);
    int getV1() const;
    int getV2() const;
    TYPE getOp() const;
    QVector<int> getConstantsId() const;
    void setConstantsId(const QVector<int> &value);

    bool getReverse() const;
};

#endif // TREECONDITION_H
