#ifndef MUTABLEELEMENT_H
#define MUTABLEELEMENT_H


class MutableElement
{
public:
    virtual ~MutableElement(){}
    virtual unsigned long long hash()const=0;
};

#endif // MUTABLEELEMENT_H
