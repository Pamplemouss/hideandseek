#ifndef TREENODE_H
#define TREENODE_H

#include "mutableelement.h"
#include "treecondition.h"
#include "treeio.h"
#include <QHash>
#include <QVector>


class NeuralTree;
class DrawableTreeNode;

class TreeNode: public MutableElement
{
public:
    TreeNode(const QVector<unsigned long long> &parents, NeuralTree *parentTree, QVector<TreeIO> *inputs,  QVector<TreeIO> *actions, const QString &parentName, bool enableOrGate, TreeNode* trueNode=nullptr, TreeNode* falseNode=nullptr, int value=-1);
    TreeNode(const TreeNode& t, NeuralTree *parentTree, bool overrideId=false);
    static TreeNode deepCopy(const TreeNode& t);

    unsigned long long hash()const;

    void draw(QPainter *p, int width, int height)const;

    virtual ~TreeNode();

    bool isTerminal()const;
    int trasverse(int id);
    void trasverseMutate(int id, double chance, double createChance,  bool shouldTransform=true, int orGateDepth=3);
    int getSize() const;

    void deleteSubtree(const TreeNode *parent);
    void crossover(const TreeNode *other, double probability, int depth=8);
    void crossover(const TreeNode *other);

    QHash<QString, double> getComposition()const;
    void addParent();
    void removeRandomParent();
    int getParentCounts();

    void mutate(int id);
    void setInputs(QVector<TreeIO> *value);
    void setActions(QVector<TreeIO> *value);
    TreeNode *getTrueNode() const;
    TreeNode *getFalseNode() const;
    QString getParentName() const;

    unsigned long long getId() const;

    bool canPassFromNode(const TreeNode *node);
    bool canBeDeleted()const;

    void registerParent(const TreeNode* parent);
    void unregisterParent(const TreeNode* parent);
    bool isParentOf(const TreeNode *child);

    void replaceBranch(TreeNode *child, bool isTrueNode);
    void getNodesAtDepth(int depth, QVector<DrawableTreeNode> &nodes)const;
    QVector<unsigned long long> getParentsIds() const;

    double getMutationOverTransformationRatio() const;
    void setMutationOverTransformationRatio(double value);

private:

    TreeNode(const TreeNode& t) = delete;
    unsigned long long hashCode()const;

    QVector<unsigned long long> parents;


    TreeNode* trueNode;
    TreeNode* falseNode;



    int value;

    int lastTraversalId;
    double mutationOverTransformationRatio=0.75;

    QVector<TreeIO> *inputs;
    QVector<TreeIO> *actions;
    TreeCondition *condition;

    const QString parentName;

    void addSubnode(bool isTrueNode, bool enableOrGate);
    void removeDirectSubnode(bool isTrueNode);

    NeuralTree *parentTree;
    unsigned long long id;

    static unsigned long long generateId();

    static unsigned long long ids;



};

#endif // TREENODE_H
