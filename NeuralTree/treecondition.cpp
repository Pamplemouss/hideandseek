
#include "treecondition.h"
#include "utils/utils.h"
#include "utils/random.h"
#include <QtDebug>
#include "treenode.h"

TreeCondition::TreeCondition(QVector<TreeIO> *inputs,  QVector<TreeIO> *actions, TreeNode *parentNode, const TreeCondition &c):
    inputs(inputs),
    actions(actions),
    v1(c.v1), v2(c.v2), op(c.op),reverse(c.reverse),
    parentNode(parentNode)
{

}

TreeCondition::TreeCondition(QVector<TreeIO> *inputs,  QVector<TreeIO> *actions, TreeNode *parentNode, bool enableOrGate):
    inputs(inputs),
    actions(actions),
    v1(Utils::randomExclude(inputs->size())),
    v2(Utils::randomExclude(inputs->size(), {v1})),
    reverse(false),
    parentNode(parentNode)
{
    if(enableOrGate){
        op = TYPE(Utils::randomExclude(4));
    }
    else{
        op = TYPE(Utils::randomExclude(4, {OR}));
    }
}

void TreeCondition::draw(QPainter *p) const
{
    QString opString = "UNKNOWN";
    if(!reverse){
        switch (op) {
            case TreeCondition::OR:
                opString="|";
                break;
            case TreeCondition::A:
                opString=">";
                break;
            case TreeCondition::AE:
                opString=">=";
                break;
            case TreeCondition::E:
                opString="==";
                break;

        }
    }
    else{
        switch (op) {
            case TreeCondition::OR:
                opString="|";
                break;
            case TreeCondition::A:
                opString="<=";
                break;
            case TreeCondition::AE:
                opString="<";
                break;
            case TreeCondition::E:
                opString="!=";
                break;
        }
    }
    p->drawText(0,0,QString(inputs->at(v1).getName() + " " + opString + " " + inputs->at(v2).getName()));
}

TreeCondition::~TreeCondition()
{

}

bool TreeCondition::isTrue()
{
    bool res = false;
    switch (this->op) {
    case TreeCondition::OR:
        // OR is always true, since the condition is always valid when we reach the instruction
        return true;
        break;
    case TreeCondition::A:
        res = inputs->at(v1).getValue()>inputs->at(v2).getValue();
        break;
    case TreeCondition::AE:
        res = inputs->at(v1).getValue()>=inputs->at(v2).getValue();
        break;
    case TreeCondition::E:
        res = inputs->at(v1).getValue()==inputs->at(v2).getValue();
        break;

    }

    return reverse ? !res : res;
}

void TreeCondition::setOp(const TYPE &value)
{
    op = value;
}

void TreeCondition::setV1(int offset)
{
    v1 = offset;
}

void TreeCondition::setV2(int offset)
{
    v2 = offset;
}

unsigned long long TreeCondition::hash() const
{
    return 10003 * (reverse ? 598 : 1238) * (unsigned long long)(pow(pow(v1, v2), double(op)));
}

bool TreeCondition::getReverse() const
{
    return reverse;
}

QVector<int> TreeCondition::getConstantsId() const
{
    return constantsId;
}

void TreeCondition::setConstantsId(const QVector<int> &value)
{
    constantsId = value;
}

TreeCondition::TYPE TreeCondition::getOp() const
{
    return op;
}

int TreeCondition::getV2() const
{
    return v2;
}

int TreeCondition::getV1() const
{
    return v1;
}

void TreeCondition::setActions(QVector<TreeIO> *value)
{
    actions = value;
}

void TreeCondition::setInputs(QVector<TreeIO> *value)
{
    inputs = value;
}

void TreeCondition::mutate(bool reverseCondition){
    Random random;
    // different mutation style if we are OR or not
    if(this->op == OR){
        //qFatal("OR must be disabled");
        if(random.d()<0.5){
            parentNode->addParent();
        }
        else{
            parentNode->removeRandomParent();
        }
    }
    else{
        bool previousState = this->isTrue();
        const double possibleMutations = 4.0;
        do{
            double r = random.d();
            if(r<1.0/possibleMutations){
                // mutate v1
                v1 = Utils::randomExclude(inputs->size(), QVector<int>({v1, v2}));
            }else if (r<2.0/possibleMutations) {
                // mutate v2
                v2 = Utils::randomExclude(inputs->size(), QVector<int>({v1, v2}));
            }
            else if (r<3.0/possibleMutations) {
                // change reverse state
                reverse = !reverse;
            }
            else{
                op = TYPE(Utils::randomExclude(4, {int(op) ,int(TreeCondition::OR)})); // allow a condition to become a OR gate
            }
        }while ((inputs->at(v1).getType()==TreeIO::CONSTANT && inputs->at(v2).getType()==TreeIO::CONSTANT) || (reverseCondition && this->isTrue()==previousState)); // prevent from comparing two constants which is useless
            //}while (this->isTrue() == previousState); // do mutation until we return a different condition than previously
    }
}
