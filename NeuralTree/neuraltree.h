#ifndef NEURALTREE_H
#define NEURALTREE_H

#include "treeio.h"
#include "treenode.h"

#include <QHash>
#include <QMutex>
#include <QPainter>
#include <QString>

class DrawableTreeNode;

class NeuralTree
{
public:
    NeuralTree(QVector<TreeIO> *inputs, QVector<TreeIO> *actions, const QString &name);
    NeuralTree(const NeuralTree &t);
    ~NeuralTree();

    double passForward();
    void mutate(bool allBranches=true);
    void backpropagate(const QVector<TreeIO> &desiredActions);

    QVector<TreeIO> *getInputs() const;

    QVector<TreeIO> *getActions() const;

    void crossover(const NeuralTree &other);
    TreeNode *findRandomNode(double probability) const;

    void setInputs(QVector<TreeIO> *value);

    void setActions(QVector<TreeIO> *value);

    int getLength() const;

    int getTargetSize() const;
    void setTargetSize(int value);
    double getAvgBranchLength() const;

    QHash<QString, double> getComposition() const;
    void refreshComposition();

    TreeNode* getNodeFromId(unsigned long long id);
    void registerNode(TreeNode *node);
    void removeNode(TreeNode *node);
    bool hasRegistered(unsigned long long id)const;

    unsigned long long getRandomTreeNodeId(unsigned long long exclude);
    unsigned long long hash()const;

    void addParentLater(unsigned long long parent, unsigned long long child);

    void draw(QPainter *painter);

private:
    QMutex mutex;
    QVector<TreeIO> *rawInputs;
    TreeNode *rootNode;
    int lastId;
    QVector<TreeIO> *inputs;
    QVector<TreeIO> *actions;
    int length;
    int targetSize;
    const QString name;
    QHash<QString, double> composition;
    QHash<unsigned long long, TreeNode *> nodes;

    QHash<unsigned long long, unsigned long long> addParentLaterBuffer;

    void computeAddParentLater();

    QVector<QVector<DrawableTreeNode>> drawableNodes;

    void updateDrawables();



};

#endif // NEURALTREE_H
