#include "treeio.h"

TreeIO::TreeIO(TYPE t, double value, const QString &name):
    value(value),
    type(t),
    name(name)
{

}

double TreeIO::getValue() const
{
    return value;
}

double TreeIO::operator=(double v)
{
    setValue(v);
    return value;
}

void TreeIO::setValue(double v)
{
    value = v;
}

TreeIO::TYPE TreeIO::getType() const
{
    return type;
}

QString TreeIO::getName() const
{
    return name;
}

void TreeIO::setName(const QString &value)
{
    name = value;
}
