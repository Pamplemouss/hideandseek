#include "drawabletreenode.h"
#include "neuraltree.h"

#include <utils/random.h>
#include <QtMath>
#include <utils/utils.h>
NeuralTree::NeuralTree(QVector<TreeIO> *inputs, QVector<TreeIO> *actions, const QString &name):
    rawInputs(new QVector<TreeIO>(*inputs)),
    lastId(0),
    inputs(inputs),
    actions(actions),
    length(1),
    targetSize(10),
    name(name)
{
    rootNode = new TreeNode({}, this, rawInputs, actions, name, false);
}

NeuralTree::NeuralTree(const NeuralTree &t):
    rawInputs(new QVector<TreeIO>(*t.rawInputs)),
    lastId(t.lastId),
    inputs(t.inputs),
    actions(t.actions),
    targetSize(t.targetSize),
    name(t.name)
{
    rootNode = new TreeNode(*t.rootNode, this);
    length = rootNode->getSize();
}

NeuralTree::~NeuralTree()
{
    rootNode->deleteSubtree(nullptr);
    delete  rootNode;
    delete rawInputs;
}

double NeuralTree::passForward()
{
    lastId++;

    for(int i=0;i<inputs->size();i++){
        (*rawInputs)[i] = inputs->at(i);
    }

    return rootNode->trasverse(lastId);
}

void NeuralTree::mutate(bool allBranches)
{
    QMutexLocker l(&mutex);
    double createProba = 1.0-(double(this->getLength())/(targetSize*2.0));
    // always create until 3/4 of target size is achieved
    if(this->getLength()<targetSize*0.75){
        createProba=1.0;
    }

     rootNode->trasverseMutate(allBranches ? -1 : lastId, 2.0/this->getLength(), createProba, true, 2);
     //computeAddParentLater();
     length = rootNode->getSize();

     drawableNodes.clear();
}
// disable warning temporarily
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
void NeuralTree::backpropagate(const QVector<TreeIO> &desiredActions)
{
    qFatal("backpropagate(): Not implemented yet");
}
#pragma GCC diagnostic pop

QVector<TreeIO> *NeuralTree::getInputs() const
{
    return inputs;
}

QVector<TreeIO> *NeuralTree::getActions() const
{
    return actions;
}

void NeuralTree::crossover(const NeuralTree &other)
{
    double crossoverProba = 1.0/getAvgBranchLength();
    TreeNode *sourceNode = other.findRandomNode(crossoverProba);

    rootNode->crossover(sourceNode, crossoverProba, 3);
}

TreeNode *NeuralTree::findRandomNode(double probability)const
{
    TreeNode * res=nullptr;
    Random random;
    TreeNode *currentNode = rootNode;
    do{
        if(random.d()<probability){
            res = currentNode;
        }else{
            if(random.d()<0.5){
                currentNode = currentNode->getTrueNode();
            }
            else{
                currentNode = currentNode->getFalseNode();
            }
        }
    }while(currentNode!=nullptr && res==nullptr);

    if(currentNode==nullptr){
        res = findRandomNode(probability);
    }

    return res;
}

void NeuralTree::setInputs(QVector<TreeIO> *value)
{
    inputs = value;
    delete rawInputs;
    rawInputs = new QVector<TreeIO>(*value);
    rootNode->setInputs(inputs);
}

void NeuralTree::setActions(QVector<TreeIO> *value)
{
    actions = value;
    rootNode->setActions(actions);
}

int NeuralTree::getLength() const
{
    return length;
}

int NeuralTree::getTargetSize() const
{
    return targetSize;
}

void NeuralTree::setTargetSize(int value)
{
    if(value>targetSize){
        targetSize = value;
    }
}

double NeuralTree::getAvgBranchLength() const
{
    int lengthSum=0;
    int treeCover = qCeil(this->getLength()/10.0);
    for(int i=0;i<treeCover;i++){
        int depth=0;
        Random random;
        TreeNode *currentNode = rootNode;
        do{
            if(random.d()<0.5){
                currentNode = currentNode->getTrueNode();
            }
            else{
                currentNode = currentNode->getFalseNode();
            }
            depth++;
        }while(currentNode!=nullptr);
        lengthSum+=depth;
    }

    return double(lengthSum)/treeCover;
}

QHash<QString, double> NeuralTree::getComposition() const
{
    return composition;
}

void NeuralTree::refreshComposition()
{
    double treeSize = this->getLength();

    composition = rootNode->getComposition();
    QMutableHashIterator<QString, double> it(composition);
    while(it.hasNext()){
        it.next();
        it.value()/=treeSize;
    }
}

TreeNode *NeuralTree::getNodeFromId(unsigned long long id)
{
    return this->nodes.value(id);
}

void NeuralTree::registerNode(TreeNode *node)
{
    if(this->nodes.contains(node->getId())){
        qFatal("RegisterNode id already registered");
    }

    this->nodes.insert(node->getId(), node);
}

void NeuralTree::removeNode(TreeNode *node)
{
    if(!this->nodes.contains(node->getId())){
        qFatal("removeNode id is not registered");
    }

    this->nodes.remove(node->getId());
}

bool NeuralTree::hasRegistered(unsigned long long id) const
{
    return this->nodes.contains(id);
}

unsigned long long NeuralTree::getRandomTreeNodeId(unsigned long long exclude)
{
    unsigned long long res;
    Random r;
    do{
        int selectedElement = r.i(nodes.size());
        auto it = nodes.begin();

        std::advance(it, selectedElement);

        res = it.key();

    }while(res==exclude);

    return res;
}

unsigned long long NeuralTree::hash() const
{
    return rootNode->hash();
}

void NeuralTree::addParentLater(unsigned long long parent, unsigned long long child)
{
    addParentLaterBuffer.insert(parent, child);
}

void NeuralTree::draw(QPainter *painter)
{
    QMutexLocker l(&mutex);
    QFont font;
    font.setPixelSize(9);

    painter->setFont(font);

    painter->drawText(4,10, "Hash: " + QString::number(this->hash()));
    painter->drawText(4,20, "Size: " + QString::number(this->getLength()) + " / "  + QString::number(this->getTargetSize()));
    painter->translate(0, 25);
    updateDrawables();

    const int conditionWidth=100;
    const int conditionHeight=25;

    // draw links
    for(int y=0;y<drawableNodes.size();y++){
        const QVector<DrawableTreeNode> &nodeCols = drawableNodes[y];
        for(int x=0;x<nodeCols.size();x++){
            nodeCols[x].drawLinks(painter);
        }
    }
    // draw boxes
    for(int y=0;y<drawableNodes.size();y++){
        const QVector<DrawableTreeNode> &nodeCols = drawableNodes[y];
        for(int x=0;x<nodeCols.size();x++){
            nodeCols[x].draw(painter);
        }
    }
}

void NeuralTree::computeAddParentLater()
{
    qFatal("computeAddParentLater() disabled");

    for(auto & k: addParentLaterBuffer.keys()){

        if(nodes.contains(k) && nodes.contains(addParentLaterBuffer[k])){
            TreeNode *p = this->nodes[k];
            TreeNode *child = this->nodes[addParentLaterBuffer[k]];

            if(!p->isParentOf(child)){
                Random r;

                // replace branch with ourself
                p->replaceBranch(child, r.d()<0.5);
            }
        }
    }
    addParentLaterBuffer.clear();
}

void NeuralTree::updateDrawables()
{
    if(drawableNodes.isEmpty()){

        // retrieve all nodes in rows
        int depth=0;
        bool reachedBottom=false;
        int maxRowSize=0;
        const int minSpacing=105;
        do{
            QVector<DrawableTreeNode> currentRow;
            rootNode->getNodesAtDepth(depth, currentRow);

            if(currentRow.isEmpty()){
                reachedBottom=true;
            }

            if(currentRow.size()>maxRowSize){
                maxRowSize = currentRow.size();
            }

            drawableNodes.append(currentRow);
            depth++;
        }while(!reachedBottom);

        // compute positions
        for(int y=0;y<drawableNodes.size();y++){
            QVector<DrawableTreeNode> &currentRow = drawableNodes[y];
            for(int x=0;x<currentRow.size();x++){

                int xCoord = 0;
                currentRow[x].setCoords((double(x)+0.5)*(double(maxRowSize)/currentRow.size()) * minSpacing, y*minSpacing/2);
            }
        }

        // link parents
        for(int y=drawableNodes.size()-1;y>0;y--){
            QVector<DrawableTreeNode> &currentRow = drawableNodes[y];
            for(int x=0;x<currentRow.size();x++){
                DrawableTreeNode &currentItem = currentRow[x];
                const TreeNode *currentItemNode = currentItem.getNode();
                QVector<unsigned long long> terminalNodeParent( {currentItemNode->getId()});

                auto parentIds = currentItem.isTerminal() ? terminalNodeParent : currentItemNode->getParentsIds();

                if(currentItemNode){
                    // look for all parents
                    for( auto parentId : parentIds){
                        DrawableTreeNode *parentNode=nullptr;


                        // optimisation: look in previous row as it is likely that the parent will be there
                        if(y>0){
                            for(int x2=0;x2<drawableNodes[y-1].size();x2++){
                                const DrawableTreeNode &potentialNode = drawableNodes[y-1][x2];

                                if(potentialNode.getNode() && potentialNode.getNode()->getId() == parentId){
                                    parentNode = &drawableNodes[y-1][x2];
                                    break;
                                }
                            }
                        }
                        // not found ? look everywhere
                        if(!parentNode){
                            for(int y2=0;y2<drawableNodes.size() && !parentNode;y2++){
                                for(int x2=0;x2<drawableNodes[y2].size();x2++){
                                    const DrawableTreeNode &potentialNode = drawableNodes[y2][x2];

                                    if(potentialNode.getNode() && potentialNode.getNode()->getId() == parentId){
                                        parentNode = &drawableNodes[y2][x2];
                                        break;
                                    }
                                }
                            }
                        }
                        if(parentNode){
                            currentItem.addParent(parentNode);
                        }
                    }
                }
            }
        }
    }
}

