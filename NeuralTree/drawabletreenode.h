#ifndef DRAWABLETREENODE_H
#define DRAWABLETREENODE_H

#include "treenode.h"



class DrawableTreeNode
{
public:
    DrawableTreeNode(const TreeNode *node, const TreeIO *out=nullptr);
    void draw(QPainter *p) const;
    void drawLinks(QPainter *p)const;
    void setCoords(int x, int y);

    void addParent(const DrawableTreeNode* parent);

    const TreeNode *getNode() const;
    bool isTerminal()const;

private:
    int getCenterX()const;
    int getCenterY()const;

    const TreeNode *node;
    const TreeIO *out;
    QVector<const DrawableTreeNode*> parents;
    int x;
    int y;
    const int width=100;
    const int height=20;
};

#endif // DRAWABLETREENODE_H
