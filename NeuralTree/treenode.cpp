#include "treenode.h"
#include "utils/utils.h"
#include "utils/random.h"

#include "drawabletreenode.h"
#include "neuraltree.h"

unsigned long long TreeNode::ids=0;

TreeNode::TreeNode(const QVector<unsigned long long> &parents, NeuralTree *parentTree, QVector<TreeIO> *inputs,  QVector<TreeIO> *actions, const QString &parentName, bool enableOrGate, TreeNode *trueNode, TreeNode *falseNode, int value):
    parents(parents),
    trueNode(trueNode),
    falseNode(falseNode),
    value(value == -1 ? Utils::randomExclude(actions->size()) : value),
    lastTraversalId(-1),
    inputs(inputs),
    actions(actions),
    condition(new TreeCondition(inputs, actions, this, enableOrGate)),
    parentName(parentName),
    parentTree(parentTree),
    id(generateId())
{
    parentTree->registerNode(this);
}

TreeNode::TreeNode(const TreeNode &t, NeuralTree *parentTree, bool overrideId):
    trueNode(nullptr),
    falseNode(nullptr),
    value(t.value),
    lastTraversalId(t.lastTraversalId),
    inputs(t.inputs),
    actions(t.actions),
    parentName(t.parentName),
    parentTree(parentTree),
    id(overrideId ? generateId() : t.id)
{
    condition = new TreeCondition(inputs, actions, this, *t.condition);
    parentTree->registerNode(this);

    if(t.trueNode != nullptr){
        // connect to already created node if it exists
        if(parentTree->hasRegistered(t.trueNode->id) && !overrideId){
            this->trueNode = parentTree->getNodeFromId(t.trueNode->id);
        }
        else{
            this->trueNode = new TreeNode(*t.trueNode, parentTree, overrideId);
        }
        this->trueNode->registerParent(this);
    }
    if(t.falseNode != nullptr){
        if(parentTree->hasRegistered(t.falseNode->id) && !overrideId){
            this->falseNode = parentTree->getNodeFromId(t.falseNode->id);
            //
        }else{
            this->falseNode = new TreeNode(*t.falseNode, parentTree, overrideId);
        }
        this->falseNode->registerParent(this);
    }
}

unsigned long long TreeNode::hash() const
{
    unsigned long long h = hashCode();
    if(trueNode){
        h=h*48972+trueNode->hash();
    }
    if(falseNode){
        h=h*4897298+falseNode->hash();
    }
    return h;
}

void TreeNode::draw(QPainter *p, int width, int height) const
{
    p->save();
    p->translate(5, height/2);

    condition->draw(p);
    p->restore();
}


TreeNode::~TreeNode()
{
    Q_ASSERT_X(this->canBeDeleted(), "~TreeNode()", "Trying to delete a Node which should not be deleted");
    parentTree->removeNode(this);
    if(condition!=nullptr){
        delete condition;
    }
}

bool TreeNode::isTerminal() const
{
    return trueNode==nullptr || falseNode==nullptr;
}

int TreeNode::trasverse(int id)
{
    int res;
    this->lastTraversalId = id;


    if(this->condition->isTrue()){
        if(this->trueNode !=nullptr){
            res = this->trueNode->trasverse(id);
        }
        else{
            res = value;
        }
    }
    else{
        if(this->falseNode !=nullptr){
            res = this->falseNode->trasverse(id);
        }
        else{
            res = value;
        }
    }


    return res;
}

void TreeNode::trasverseMutate(int id, double chance, double createChance, bool shouldTransform, int orGateDepth)
{
    Random random;
    orGateDepth--;


            Q_ASSERT_X(condition!=nullptr, "trasverseMutate()", "Trasversing a Node that has been deleted");
    if(lastTraversalId==id || id==-1){
        double r = random.d();
        if(r<chance){
            // do actual mutation
            double r = random.d();
            double r2 = random.d();
            if(r<mutationOverTransformationRatio || !shouldTransform){
                this->mutate(id);
            }
            else
            {
                double r3 = random.d();
                if(r3<0.5){
                    if(r2<createChance){
                        this->addSubnode(true, orGateDepth<0);
                    }
                    else{
                        removeDirectSubnode(true);
                    }
                }
                else{
                    if(r2<createChance){
                        this->addSubnode(false, orGateDepth<0);
                    }
                    else{
                        removeDirectSubnode(false);
                    }
                }
                // allow only one transformation to avoid exponential growth
                shouldTransform=false;
            }
        }

        if(trueNode!=nullptr){

            trueNode->trasverseMutate(id, chance, createChance, shouldTransform, orGateDepth);
        }

        if(falseNode!=nullptr){

            falseNode->trasverseMutate(id, chance, createChance, shouldTransform, orGateDepth);
        }

    }


}

int TreeNode::getSize() const
{
    // size is represented by the number of parents (connections between nodes)
    int l = parents.size();
    if(trueNode!=nullptr){
        l+=trueNode->getSize();
    }
    if(falseNode!=nullptr){
        l+=falseNode->getSize();
    }
    return l > 0 ? l : 1;
}

void TreeNode::deleteSubtree(const TreeNode *parent)
{
    Q_ASSERT_X(condition!=nullptr, "deleteSubtree()", "Node has already been deleted");

    // check if we have multiple parents, don't delete ourself in this case
    Q_ASSERT_X(parent==nullptr || parents.contains(parent->id), "deleteSubtree()", "Trying to delete a TreeNode using a parent node affiliated with itself");

    // remove parent from list
    if(parent != nullptr){
        this->unregisterParent(parent);
    }

    // delete branch if it doesn't have any parent
    if(parents.size()==0){
        if(trueNode!=nullptr){
            trueNode->deleteSubtree(this);
            if(trueNode->canBeDeleted()){
                delete trueNode;
            }
            trueNode=nullptr;
        }
        if(falseNode!=nullptr){
            falseNode->deleteSubtree(this);
            if(falseNode->canBeDeleted()){
                delete falseNode;
            }
            falseNode=nullptr;
        }

        delete condition;
        condition=nullptr;
    }

}

void TreeNode::crossover(const TreeNode *other, double probability, int depth)
{
    //qFatal("Must implement proper multi parent crossover");
    Random random;
    // do crossover on this node ?
    if(random.d()<probability && depth<=0){
        this->crossover(other);
    }
    // randomly explore childs until we do a crossover
    else{
        if(random.d()<0.5 && trueNode!=nullptr){
            trueNode->crossover(other, probability, depth-1);

        }
        else if(falseNode!=nullptr){
            falseNode->crossover(other, probability, depth-1);
        }
    }


}

void TreeNode::crossover(const TreeNode *other)
{
    //qFatal("Must implement proper multi parent crossover");
    Random random;
    TreeNode* oldNode=nullptr;
    TreeNode* newNode=nullptr;
    if(random.d()<0.5){
        oldNode = trueNode;
    }
    else{
        oldNode = falseNode;
    }

    if(oldNode!=nullptr){
        oldNode->deleteSubtree(this);
        if(oldNode->canBeDeleted()){
            delete oldNode;
        }
    }
    // maybe this node already exists
    if(parentTree->hasRegistered(other->getId())){
        newNode = parentTree->getNodeFromId(other->getId());
    }
    else {
        newNode=new TreeNode(*other, parentTree, true);
    }

    if(oldNode==trueNode){
        trueNode=newNode;
    }
    else{
        falseNode=newNode;
    }
    newNode->setInputs(inputs);
    newNode->setActions(actions);
    newNode->registerParent(this);





}

// return the DNA composition
QHash<QString, double> TreeNode::getComposition() const
{
    QHash<QString, double> res;

    if(trueNode!=nullptr){
        res=trueNode->getComposition();
    }

    if(falseNode!=nullptr){
        QHash<QString, double> tmp=falseNode->getComposition();

        // merge tmp & res
        if(!res.isEmpty()){
            QHash<QString, double>::const_iterator i = tmp.constBegin();
            while (i != tmp.constEnd()) {
                if(res.contains(i.key())){
                    res[parentName]+=i.value();
                }
                else{
                    res.insert(i.key(), i.value());
                }
                ++i;
            }
        }
        else{
            res=tmp;
        }
    }


    if(res.contains(parentName)){
        res[parentName]+=this->parents.size();
    }
    else{
        res.insert(parentName, this->parents.size());
    }

    return res;
}

void TreeNode::removeRandomParent()
{
    if(getParentCounts()>1){
        auto id = parents[Utils::randomExclude(parents.size())];
        TreeNode * p = this->parentTree->getNodeFromId(id);// parents[Utils::randomExclude(getParentCounts())];

        // disconnect parent from ourself
        if(p->trueNode == this){
            p->trueNode = nullptr;
        }
        else if(p->falseNode == this){
            p->falseNode = nullptr;
        }
        else{
            qFatal("Could not find ourself in parent branch");
        }

        // remove parent in parent list
        this->unregisterParent(p);
    }
}

int TreeNode::getParentCounts()
{
    return parents.size();
}

void TreeNode::addParent()
{
    TreeNode *p = nullptr;
    const int maxTries = 5; // get out of infinite loop if no solutions can be found
    int  tries=0;
    // repeat until parent doesn't create a circular dependency, TODO: find a  more optimized way
    do{
        auto id = parentTree->getRandomTreeNodeId(this->id);
        p = parentTree->getNodeFromId(id);
        tries++;
    }while((this->canPassFromNode(p) || this->parents.contains(p->id)) && tries<maxTries );

    // don't add parent if we failed to find one
    if(tries<maxTries && p!=nullptr ){
        //parentTree->addParentLater(p->id, this->id);
        Random r;
        p->replaceBranch(this, r.d()<0.5);
    }
}

QString TreeNode::getParentName() const
{
    return parentName;
}

TreeNode *TreeNode::getFalseNode() const
{
    return falseNode;
}

TreeNode *TreeNode::getTrueNode() const
{
    return trueNode;
}


void TreeNode::setActions(QVector<TreeIO> *value)
{
    actions = value;
    if(trueNode!=nullptr){
        trueNode->setActions(actions);
    }
    if(falseNode!=nullptr){
        falseNode->setActions(actions);
    }

    condition->setActions(actions);
}

void TreeNode::setInputs(QVector<TreeIO> *value)
{
    inputs = value;
    if(trueNode!=nullptr){
        trueNode->setInputs(inputs);
    }
    if(falseNode!=nullptr){
        falseNode->setInputs(inputs);
    }
    condition->setInputs(inputs);
}

// insert a subnode between current node if it exists
void TreeNode::addSubnode(bool isTrueNode, bool enableOrGate)
{
    TreeNode *createdNode;
    TreeNode* oldNode;

    // TODO: Condition OR is always true, don't add false node
    if(isTrueNode){
        oldNode = trueNode;

        trueNode = createdNode = new TreeNode({this->id}, parentTree, inputs, actions, parentName, enableOrGate, oldNode, nullptr, Utils::randomExclude(actions->size()));

    }
    else{
        oldNode = falseNode;

        falseNode = createdNode = new TreeNode({this->id}, parentTree, inputs, actions, parentName, enableOrGate, nullptr, oldNode, Utils::randomExclude(actions->size()));

    }

    // createdNode is now the direct parent  of oldNode
    if(oldNode){
        oldNode->registerParent(createdNode);
        oldNode->unregisterParent(this);
    }
}

// remove a child node but not child node's childs (connect ourself directly to child node's childs)
void TreeNode::removeDirectSubnode(bool isTrueNode)
{
    TreeNode *oldNode=nullptr;
    TreeNode *oldNodeChoosenSubNode=nullptr;
    //TreeNode *oldNodeOtherSubNode=nullptr;

    // connect ourself directly to subnode's child
    if(isTrueNode && trueNode!=nullptr){
        oldNode = trueNode;
        oldNodeChoosenSubNode = oldNode->trueNode;
        //oldNodeOtherSubNode=oldNode->falseNode;

    }
    else if(!isTrueNode && falseNode!=nullptr){
        oldNode = falseNode;
        oldNodeChoosenSubNode = oldNode->falseNode;
        //oldNodeOtherSubNode=oldNode->trueNode;
    }
    // do nothing if we are already a parent of the choosen node
    if(!oldNodeChoosenSubNode || !this->isParentOf(oldNodeChoosenSubNode)){

        if(isTrueNode){
            trueNode = oldNodeChoosenSubNode;
        }
        else{
            falseNode = oldNodeChoosenSubNode;
        }
        // update parent if new connected node is not null
        if(oldNodeChoosenSubNode){
            oldNodeChoosenSubNode->registerParent(this);
        }

        // remove parent link && delete unnecessary parts of the tree if possible
        if(oldNode){
            oldNode->deleteSubtree(this);
        }

        if(oldNode){
            // delete node if it has no more parents
            if(oldNode->canBeDeleted()){
                delete oldNode;
            }
        }
    }

}

unsigned long long TreeNode::generateId()
{
    return ids++;
}

unsigned long long TreeNode::getId() const
{
    return id;
}

bool TreeNode::canPassFromNode(const TreeNode *node)
{
    bool res=false;
    if(this == node){
        // IMPORTANT: return here before doing subtrees check in order not to create an infinite loop
        return true;
    }

    if(this->trueNode){
        res |= trueNode->canPassFromNode(node);
    }
    if(!res && this->falseNode){
        res |= falseNode->canPassFromNode(node);
    }

    return res;
}

// check if node can be deleted (have no parents left)
bool TreeNode::canBeDeleted() const
{
    return parents.size()==0;
}

void TreeNode::registerParent(const TreeNode *parent)
{
    Q_ASSERT_X(!this->parents.contains(parent->id), "registerParent()", "Parent already registered");
    this->parents.append(parent->id);

}

void TreeNode::unregisterParent(const TreeNode *parent)
{
    Q_ASSERT_X(this->parents.contains(parent->id), "unregisterParent()", "Parent is not registered");
    this->parents.removeOne(parent->id);
}

bool TreeNode::isParentOf(const TreeNode *child)
{
    return child->parents.contains(this->id);
}

void TreeNode::replaceBranch(TreeNode *child, bool isTrueNode)
{
    TreeNode *oldNode;
    if(isTrueNode){
        oldNode = trueNode;
        this->trueNode = child;
    }
    else{
        oldNode = falseNode;
        this->falseNode = child;
    }
    // register new parent
    child->registerParent(this);

    // delete old node
    if(oldNode){
        oldNode->deleteSubtree(this);

        if(oldNode->canBeDeleted()){
            delete oldNode;
        }
    }

}

void TreeNode::getNodesAtDepth(int depth, QVector<DrawableTreeNode> &nodes)const
{
    if(depth==0){

        nodes.append(this);
    }
    else{
        if(trueNode){
            trueNode->getNodesAtDepth(depth-1, nodes);
        }
        else if(depth==1){
            nodes.append(DrawableTreeNode(this, &actions->at(value)));
        }

        if(falseNode){
            falseNode->getNodesAtDepth(depth-1, nodes);
        }
        else if(depth==1 && trueNode){
            nodes.append(DrawableTreeNode(this, &actions->at(value)));
        }
    }
}

unsigned long long TreeNode::hashCode() const
{
    unsigned long long parentHash = 0;
    for(auto pId : this->parents){
        auto *p = parentTree->getNodeFromId(pId);
        parentHash += p->hashCode();
    }
    return condition->hash() + parentHash;
}

double TreeNode::getMutationOverTransformationRatio() const
{
    return mutationOverTransformationRatio;
}

void TreeNode::setMutationOverTransformationRatio(double value)
{
    mutationOverTransformationRatio = value;
}

QVector<unsigned long long> TreeNode::getParentsIds() const
{
    return parents;
}



void TreeNode::mutate(int id)
{

    this->condition->mutate(id!=-1);

}
