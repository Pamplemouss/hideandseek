#include "drawabletreenode.h"

#include <QString>

DrawableTreeNode::DrawableTreeNode(const TreeNode *node, const TreeIO *out):
    node(node),
    out(out),
    x(0),
    y(0)
{

}


void DrawableTreeNode::draw(QPainter *p)const
{
    p->save();
    p->translate(x, y);
    p->drawRect(0,0, width, height);

    if(out){
        p->drawText(5, height/2,out->getName());
    }
    else{
         node->draw(p, width, height);

    }
    p->restore();


}

void DrawableTreeNode::drawLinks(QPainter *p) const
{
    for(const auto *parent : parents){
        p->drawLine(getCenterX(), getCenterY(), parent->getCenterX(), parent->getCenterY());
    }
}

void DrawableTreeNode::setCoords(int x, int y)
{
    this->x = x;
    this->y = y;
}

void DrawableTreeNode::addParent(const DrawableTreeNode *parent)
{
    parents.append(parent);
}

const TreeNode *DrawableTreeNode::getNode() const
{
    return node;
}

bool DrawableTreeNode::isTerminal() const
{
    return out!=nullptr;
}

int DrawableTreeNode::getCenterX() const
{
    return x + width/2;
}

int DrawableTreeNode::getCenterY() const
{
    return y + height/2;
}
