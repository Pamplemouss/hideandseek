#ifndef INPUT_H
#define INPUT_H

#include <QString>



class TreeIO
{
public:
    enum TYPE{INPUT, CONSTANT};
    TreeIO(TYPE  t=INPUT, double value=0.0, const QString &name="");

    double getValue() const;
    double operator=(double v);
    void setValue(double v);

    TYPE getType() const;

    QString getName() const;
    void setName(const QString &value);

private:
    double value;
    TYPE type;
    QString name;
};

#endif // INPUT_H
