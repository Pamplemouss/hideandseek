#include "treedisplaywindow.h"
#include "ui_treedisplaywindow.h"

TreeDisplayWindow::TreeDisplayWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TreeDisplayWindow)
{
    ui->setupUi(this);
}

TreeDisplayWindow::~TreeDisplayWindow()
{
    delete ui;
}

TreeDisplay *TreeDisplayWindow::getTreeDisplay() const
{
    return ui->widget;
}
