#include "previewdisplay.h"
#include <QPainter>
#include <QTimer>
#include <QKeyEvent>
#include <Training/Managers/trainingmanager.h>
#include "Game/gamemanager.h"
#include <QtDebug>
#include <QtGlobal>
#include <Qt>
#include <utils/bigtimer.h>
PreviewDisplay::PreviewDisplay(QWidget *parent):
    DisplayArea(parent, 60, 40, false),
    trainingCyclesPerGroup(20),
    trainingCycles(0),
    trainHiders(false),
    bestHiderScore(-1000),
    bestSeekerScore(-1000),
    totalTrainingCycles(0),
    liveEnabled(true)
{
    manager = TrainingManager::getInstance();
    render=true;

    // training signals
    connect(manager->getTrainer(), &OpponentsTrainer::newBestScore, this, &PreviewDisplay::livePreview, Qt::BlockingQueuedConnection);
    liveAI = manager->getSeekerFactory()->create("");

    // start trainer
    manager->getTrainer()->start();


    this->start();

}
PreviewDisplay::~PreviewDisplay(){
    manager->getTrainer()->exit();
    manager->getTrainer()->wait(10000);
    delete displayTimer;
    delete painter;
}
void PreviewDisplay::initializeGL()
{
    QOpenGLWidget::initializeGL();
}

void PreviewDisplay::resizeGL(int w, int h)
{
    QOpenGLWidget::resizeGL(w, h);
}

void PreviewDisplay::paintGL()
{
   beginPaint();
   QMutexLocker l(&mutex);


   QVector<TrainingMetrics> metrics;
   metrics.append(manager->getHidersTrainer()->getMetrics());
   metrics.append(manager->getSeekersTrainer()->getMetrics());

   if(render){

       if (liveEnabled) liveAI->draw(painter, width(), height());
       else specificAI->draw(painter, width(), height());
    /*
       for(int i=0;i<metrics.size();i++){
           TrainingMetrics &metric = metrics[i];
           const int metricSize = 120;
           const int gamePreviewSize = 300;
           int gamePixelSize = metricSize + 5;

           int gamesPerRow = 2;//qMax((width()-gamePreviewSize)/gamePixelSize, 1);

           int gameX = gamePreviewSize + (i%gamesPerRow)*gamePixelSize;
           int gameY = (i/gamesPerRow)*gamePixelSize*2;
           metric.draw(painter, metricSize, gameX, gameY);
       }
       */

   }


   endPaint();
}

void PreviewDisplay::restartGame()
{
    // start a new game using our best AIs
    //displayGame->restart();
}

void PreviewDisplay::step()
{
    beginStep();

    QMutexLocker l(&mutex);

    if(render){
        if (liveEnabled) liveAI->step();
        else specificAI->step();
    }
    endStep();
}

void PreviewDisplay::specificPreview(Trainable *trainable)
{
    QMutexLocker l(&mutex);
    liveEnabled = false;
    specificAI = manager->getFactory(*trainable)->copy(*trainable);
}

void PreviewDisplay::livePreview(Trainable *trainable)
{
    QMutexLocker l(&mutex);
    manager->getFactory(*liveAI)->remove(liveAI);
    liveAI = manager->getFactory(*trainable)->copy(*trainable);
}

void PreviewDisplay::backToLive()
{
    QMutexLocker l(&mutex);
    manager->getFactory(*specificAI)->remove(specificAI);
    liveEnabled = true;
}


