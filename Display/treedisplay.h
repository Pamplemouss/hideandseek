#ifndef BRAINDISPLAY_H
#define BRAINDISPLAY_H

#include "displayarea.h"

#include <NeuralTree/neuraltree.h>

#include <Training/trainable.h>

#include <AI/hidenseekai.h>



class TreeDisplay : public DisplayArea
{
public:
    TreeDisplay(QWidget *parent);

public slots:
    void updateTree(const Trainable *t);
private:
    Trainable *trainable;

    // QOpenGLWidget interface
protected:
    void paintGL();
};

#endif // BRAINDISPLAY_H
