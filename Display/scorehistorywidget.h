#ifndef SCOREHISTORYWIDGET_H
#define SCOREHISTORYWIDGET_H

#include "scorehistorydisplay.h"
#include "QScrollBar"
#include "QBoxLayout"
#include "QPushButton"

class ScoreHistoryWidget: public QWidget
{
    Q_OBJECT
public:
    ScoreHistoryWidget(QWidget *w);
    ~ScoreHistoryWidget();

protected:
    ScoreHistoryDisplay *fresque;
    QScrollBar *scrollbar;
    QHBoxLayout *HLayout;
    QVBoxLayout *VLayout1;
    QVBoxLayout *VLayout2;
    QPushButton *displayButton;
    QPushButton *mostRecentButton;
    ScoreHistory *scoreHistory;

public slots:
    void newData(int size);
    void displayAI();
    void mostRecentButtonClicked();

signals:
    void display(Trainable* ai);
    void backToLive();
};

#endif // SCOREHISTORYWIDGET_H
