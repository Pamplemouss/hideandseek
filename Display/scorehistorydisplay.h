#ifndef SCOREHISTORYDISPLAY_H
#define SCOREHISTORYDISPLAY_H

#include "displayarea.h"
#include "Training/Managers/trainingmanager.h"


class ScoreHistoryDisplay: public DisplayArea
{
    Q_OBJECT

public:
    ScoreHistoryDisplay(QWidget *w);
    int getDataPerPrint() const;
    int getHighlightIndex() const;
    int getStart() const;
    void setHighlightIndex(int value);
    void setLiveIndex(int value);

protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void leaveEvent(QEvent *event);
    void paintGL();
    ScoreHistory* getScoreHistory();
    int dataPerPrint = 100;
    int start = 0;
    int hoverIndex = -1;
    int highlightIndex = -1;
    int liveIndex = -1;

public slots:
    void setStart(int value);

signals:
    void IASelected(bool selected);
};

#endif // METRICSDISPLAY_H
