#include "scorehistorydisplay.h"
#include "QMouseEvent"
#include "QToolTip"
#include "math.h"

ScoreHistoryDisplay::ScoreHistoryDisplay(QWidget *w):
        DisplayArea(w)
{
    setMouseTracking(true);
}

void ScoreHistoryDisplay::paintGL()
{
    ScoreHistory* scoreHistory = getScoreHistory();

    beginPaint();

    double offset = width() / double(dataPerPrint);
    double x = 0;

    for (int i = 0 ; i < dataPerPrint && i < scoreHistory->getDataset().size() ; i++) {
        double h = (scoreHistory->getDataset()[i+start].getHiderScore() / 100) * height();
        double s = (scoreHistory->getDataset()[i+start].getSeekerScore() / 100) * height();
        bool seekerTraining = scoreHistory->getDataset()[i+start].wasSeekerTraining();

        if (i == (liveIndex-start)) {
            painter->fillRect(QRectF({x,0}, QSizeF(offset, h)), QColor(51, 204, 51));                                   //Golden hider score (current live)
            painter->fillRect(QRectF({x,h}, QSizeF(offset, s)), QColor(0, 153, 0));                                     //Golden seeker score (current live)
        }
        else if (i == (highlightIndex-start)) {
            painter->fillRect(QRectF({x,0}, QSizeF(offset, h)), QColor(255, 215, 0));                                   //Golden hider score (selected)
            painter->fillRect(QRectF({x,h}, QSizeF(offset, s)), QColor(209, 176, 0));                                   //Golden seeker score (selected)
        }
        else {
            painter->fillRect(QRectF({x,0}, QSizeF(offset, h)), QColor(0, 153, 255));                                   //Hider score
            painter->fillRect(QRectF({x,h}, QSizeF(offset, s)), QColor(255, 80, 80));                                   //Seeker score
        }

        if (!seekerTraining) painter->fillRect(QRectF({x+(offset/2.0)-1.0, 2}, QSizeF(2, 2)), QColor(255, 255, 255));   //Line meaning hider training
        else painter->fillRect(QRectF({x+(offset/2.0)-1,height()-4.0}, QSizeF(2, 2)), QColor(255, 255, 255));           //Line meaning seeker training

        if (i == hoverIndex) painter->fillRect(QRectF({x,0}, QSizeF(offset, height())), QColor(0, 0, 0, 100));          //Hover highlight

        x += offset;
    }

    endPaint();
}

int ScoreHistoryDisplay::getDataPerPrint() const
{
    return dataPerPrint;
}

int ScoreHistoryDisplay::getHighlightIndex() const
{
    return highlightIndex;
}

int ScoreHistoryDisplay::getStart() const
{
    return start;
}

void ScoreHistoryDisplay::mouseMoveEvent(QMouseEvent *event)
{

    hoverIndex = floor(event->localPos().x() / (width() / double(dataPerPrint)));

    if (hoverIndex >= getScoreHistory()->getDataset().size()) {
        hoverIndex = -1;
        QToolTip::hideText();
        this->setCursor(Qt::ArrowCursor);
    }
    else {
        ScoreHistory* s = getScoreHistory();
        double seekerScore = getScoreHistory()->getDataset()[start+hoverIndex].getSeekerScore();
        double hiderScore = getScoreHistory()->getDataset()[start+hoverIndex].getHiderScore();
        QString score = "<span style='color:rgb(255,80,80); font-weight:bold'>"
                + QString::number(seekerScore)
                + "</span> - <span style='color:rgb(0,153,255); font-weight:bold'>"
                + QString::number(hiderScore) + "</span>";
        QPoint point = event->screenPos().toPoint() + QPoint(0,-40); //translate point upward
        QToolTip::showText(point, score, this);
        this->setCursor(Qt::PointingHandCursor);
    }
}

void ScoreHistoryDisplay::mouseReleaseEvent(QMouseEvent *event)
{
    highlightIndex = floor(event->localPos().x() / (width() / double(dataPerPrint))) + start;

    if (highlightIndex >= getScoreHistory()->getDataset().size()) {
        highlightIndex = -1;
        emit IASelected(false);
    }
    else {
        emit IASelected(true);
    }
}

void ScoreHistoryDisplay::leaveEvent(QEvent *event)
{
    hoverIndex = -1;
}

ScoreHistory* ScoreHistoryDisplay::getScoreHistory() {
    return TrainingManager::getInstance()->getScoreHistory();
}

void ScoreHistoryDisplay::setLiveIndex(int value)
{
    liveIndex = value;
}

void ScoreHistoryDisplay::setHighlightIndex(int value)
{
    highlightIndex = value;
}

void ScoreHistoryDisplay::setStart(int value)
{
    start = value;
}
