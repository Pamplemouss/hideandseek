#ifndef FOOTPRINTSDISPLAY_H
#define FOOTPRINTSDISPLAY_H

#include "displayarea.h"



class FootPrintsDisplay : public DisplayArea
{
public:
    FootPrintsDisplay(QWidget *w);

    // QOpenGLWidget interface
protected:
    void paintGL();

};

#endif // FOOTPRINTSDISPLAY_H
