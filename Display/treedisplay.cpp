#include "treedisplay.h"

#include <Training/Managers/trainingmanager.h>

#include <MultiGameTraining/multigametrainable.h>

#include <AI/hiderai.h>

TreeDisplay::TreeDisplay(QWidget *parent):
    DisplayArea(parent, 1),
    trainable(nullptr)

{

}

void TreeDisplay::updateTree(const Trainable *t)
{
    if(trainable){
        TrainingManager::getInstance()->getFactory(*this->trainable)->remove(this->trainable);
    }

    this->trainable = TrainingManager::getInstance()->getFactory(*t)->copy(*t);
}

void TreeDisplay::paintGL()
{
    beginPaint();
    NeuralTree *tree=nullptr;
    MultiGameTrainable<SeekerAI> *multiSeeker = dynamic_cast<MultiGameTrainable<SeekerAI>*>(trainable);
    MultiGameTrainable<HiderAI>* multiHider = dynamic_cast<MultiGameTrainable<HiderAI>*>(trainable);
    HideNSeekAI *hideNSeek = dynamic_cast<HideNSeekAI*>(trainable);
    QColor color(45, 52, 54);
    QPen pen(color);
    pen.setWidth(2);
    painter->setPen(pen);

    painter->setBrush(Qt::white);
    painter->fillRect(0,0, width(), height(), Qt::white/*QColor(255, 234, 167)*/);

    if(multiSeeker){
        tree = multiSeeker->getAI()->getBrain();
    }else if(multiHider){
        tree = multiHider->getAI()->getBrain();
    }
    else if(hideNSeek){
        tree = hideNSeek->getBrain();
    }

    if(tree){
        tree->draw(painter);
    }
    endPaint();
}
