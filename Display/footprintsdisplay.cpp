#include "footprintsdisplay.h"

#include <Game/room.h>

FootPrintsDisplay::FootPrintsDisplay(QWidget *w):
    DisplayArea(w)
{

}

void FootPrintsDisplay::paintGL()
{
    beginPaint();
    Room::getCombinedFootPrintInstance()->draw(painter, Room::LIGHT);
    endPaint();
}
