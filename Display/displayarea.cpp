#include "displayarea.h"

#include <QPainter>

DisplayArea::DisplayArea(QWidget *parent, int fps, int stepsPerS, bool autoStart):
    QOpenGLWidget(parent),
    fps(fps),
    stepsPerS(stepsPerS)
{
    painter = new QPainter;
    displayTimer =  new QTimer;
    stepTimer = new QTimer;

    connect(displayTimer,SIGNAL(timeout()),this,SLOT(update()));
    displayTimer->setSingleShot(true);

    setFocusPolicy(Qt::FocusPolicy::StrongFocus);


    // launch step timer
    connect(stepTimer, SIGNAL(timeout()), this, SLOT(step()));
    stepTimer->setSingleShot(true);

    if(autoStart){
        start();
    }
}

void DisplayArea::start()
{
    displayTimer->start();

    if(stepsPerS>0){
        stepTimer->start();
    }
}

void DisplayArea::step()
{

}


void DisplayArea::beginPaint()
{
    displayElapsed.start();
    painter->begin(this);
    painter->setRenderHint(QPainter::Antialiasing);
}

void DisplayArea::endPaint()
{
    painter->end();
    displayTimer->start(qMax(int(1000.0/fps - double(displayElapsed.elapsed())), 0));
}

void DisplayArea::beginStep()
{
    stepElapsed.start();
}

void DisplayArea::endStep()
{
    stepTimer->start(qMax(int(1000.0/stepsPerS - double(stepElapsed.elapsed())), 0));
}
