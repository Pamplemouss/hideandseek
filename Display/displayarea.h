#ifndef DISPLAYAREA_H
#define DISPLAYAREA_H

#include <QElapsedTimer>
#include <QOpenGLWidget>
#include <QTimer>



class DisplayArea : public QOpenGLWidget
{
    Q_OBJECT

public:
    DisplayArea(QWidget *parent, int fps=30, int stepsPerS=0, bool autoStart=true);
    void start();

public slots:
    virtual void step();

protected:

    virtual void beginPaint();
    virtual void endPaint();

    virtual void beginStep();
    virtual void endStep();

    QPainter *painter;
    QTimer *displayTimer;
    QTimer *stepTimer;

    QElapsedTimer displayElapsed;
    QElapsedTimer stepElapsed;

    int fps;
    int stepsPerS;


};

#endif // DISPLAYAREA_H
