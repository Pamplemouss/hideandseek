#include "scorehistorywidget.h"
#include <Training/Managers/trainingmanager.h>

ScoreHistoryWidget::ScoreHistoryWidget(QWidget *w):
    QWidget(w)
{
    HLayout = new QHBoxLayout(this);
    displayButton = new QPushButton("Display", this);
    mostRecentButton = new QPushButton("Back to Live", this);
    VLayout1 = new QVBoxLayout();
    VLayout2 = new QVBoxLayout();
    fresque = new ScoreHistoryDisplay(this);
    scrollbar = new QScrollBar();

    HLayout->addLayout(VLayout1);
    HLayout->addLayout(VLayout2);
    VLayout1->addWidget(displayButton);
    VLayout1->addWidget(mostRecentButton);
    VLayout2->addWidget(fresque);
    VLayout2->addWidget(scrollbar);

    scrollbar->setOrientation(Qt::Horizontal);
    scrollbar->setRange(0,0);

    displayButton->setEnabled(false);
    displayButton->setMaximumWidth(75);
    displayButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    mostRecentButton->setEnabled(false);
    mostRecentButton->setMaximumWidth(75);
    mostRecentButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    scoreHistory = TrainingManager::getInstance()->getScoreHistory();

    connect(scoreHistory, SIGNAL(newData(int)), this, SLOT(newData(int)));
    connect(scrollbar, SIGNAL(valueChanged(int)), fresque, SLOT(setStart(int)));
    connect(fresque, SIGNAL(IASelected(bool)), displayButton, SLOT(setEnabled(bool)));
    connect(displayButton, SIGNAL(clicked()), this, SLOT(displayAI()));
    connect(mostRecentButton, SIGNAL(clicked()), this, SLOT(mostRecentButtonClicked()));
}

ScoreHistoryWidget::~ScoreHistoryWidget()
{
    delete fresque;
    delete displayButton;
    delete mostRecentButton;
    delete VLayout1;
    delete VLayout2;
    delete HLayout;
    delete scrollbar;
}

void ScoreHistoryWidget::newData(int size)
{
    bool snapRight;
    scrollbar->value() == scrollbar->maximum() ? snapRight = true : snapRight = false;

    if (size > fresque->getDataPerPrint()) {
        scrollbar->setRange(0, size - fresque->getDataPerPrint());
        if (snapRight) scrollbar->setValue(scrollbar->maximum());
    }
}

void ScoreHistoryWidget::displayAI()
{
    fresque->setLiveIndex(fresque->getHighlightIndex());
    mostRecentButton->setEnabled(true);
    emit display(scoreHistory->getDataset()[fresque->getHighlightIndex()].getAI());
}

void ScoreHistoryWidget::mostRecentButtonClicked()
{
    fresque->setLiveIndex(-1);
    mostRecentButton->setEnabled(false);
    emit backToLive();
}
