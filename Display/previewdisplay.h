#ifndef RENDERINGAREA_H
#define RENDERINGAREA_H
#include "Game/gamemanager.h"
#include "displayarea.h"
#include "hiderfactory.h"
#include "seekerfactory.h"


#include <QMutex>
#include <QOpenGLWidget>
#include <Training/Managers/trainingmanager.h>

#include <Training/genetictrainer.h>
#include <Training/opponentstrainer.h>

#include <AI/hiderai.h>
#include <AI/seekerai.h>

class PreviewDisplay : public DisplayArea
{
    Q_OBJECT
public:
    PreviewDisplay(QWidget *parent);
    ~PreviewDisplay();

    // QOpenGLWidget interface
protected:
    void initializeGL();
    void resizeGL(int w, int h);
    virtual void paintGL();


private:
    Trainable *liveAI;
    Trainable *specificAI;
    QTimer *restartTimer;
    QMutex mutex;

    double bestHiderScore;
    double bestSeekerScore;

    bool trainHiders;
    bool render;
    int trainingCycles;
    double lastBestFitness;
    int  nPreview;
    int trainingCyclesPerGroup;
    int  totalTrainingCycles;
    bool liveEnabled;

    TrainingManager *manager;


public slots:
    void restartGame();
    void step();

    void specificPreview(Trainable* trainable);
    void livePreview(Trainable* trainable);
    void backToLive();

};

#endif // RENDERINGAREA_H
