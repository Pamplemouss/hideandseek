#include "seekerfactory.h"

#include "Game/gamemanager.h"

#include <AI/hiderai.h>

SeekerFactory::SeekerFactory()
{

}

Trainable *SeekerFactory::create(const QString &name) const
{
    SeekerAI *res = new SeekerAI(name);
    this->initGame(res);

    return res;
}

Trainable *SeekerFactory::create(const Trainable &trainable)const
{
    SeekerAI *res = new SeekerAI(dynamic_cast<const SeekerAI&>(trainable));
    this->initGame(res);
    return res;
}

Trainable *SeekerFactory::copy(const Trainable &trainable) const
{
    qFatal("Not  implemented");
}

void SeekerFactory::remove(Trainable *t) const
{
    SeekerAI *seeker = dynamic_cast<SeekerAI*>(t);
    GameManager *g(seeker->getGameManager());
    qDeleteAll(g->getHiders());
    qDeleteAll(g->getSeekers());
    delete g;
}


void SeekerFactory::setOpponents(const QVector<Trainable *> &value)
{
    qDeleteAll(opponents);
    opponents.clear();
    for(auto *v : value){
        opponents.append(new HiderAI(*dynamic_cast<HiderAI*>(v)));
    }
}

void SeekerFactory::initGame(SeekerAI *newHider)const
{
    GameManager *g = new GameManager();

    // deep copy opponents
    QVector<MovingEntity *> copiedOpponents;
    for(auto *o : opponents){
        HiderAI *m = new HiderAI(*dynamic_cast<HiderAI*>(o));
        copiedOpponents.push_back(m);
    }

    // notify game manager of his new players
    g->setSeekers({newHider});
    g->setHiders(copiedOpponents);

    g->restart();
}

int SeekerFactory::getDesiredOpponentCount() const
{
    return 1;
}
