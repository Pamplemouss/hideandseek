#ifndef TREEDISPLAYWINDOW_H
#define TREEDISPLAYWINDOW_H

#include <QMainWindow>

#include <Display/treedisplay.h>

namespace Ui {
class TreeDisplayWindow;
}

class TreeDisplayWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit TreeDisplayWindow(QWidget *parent = nullptr);
    ~TreeDisplayWindow();
    TreeDisplay *getTreeDisplay()const;

private:
    Ui::TreeDisplayWindow *ui;
};

#endif // TREEDISPLAYWINDOW_H
