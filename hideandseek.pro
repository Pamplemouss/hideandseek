QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AI/seekerai.cpp \
    Display/displayarea.cpp \
    Display/footprintsdisplay.cpp \
    Display/previewdisplay.cpp \
    Display/scorehistorydisplay.cpp \
    Display/scorehistorywidget.cpp \
    Display/treedisplay.cpp \
    Entities/eye.cpp \
    Game/floortile.cpp \
    Game/room.cpp \
    MultiGameTraining/multigametrainable.cpp \
    NeuralTree/drawabletreenode.cpp \
    ScoreHistory/scoredatapoint.cpp \
    ScoreHistory/scorehistory.cpp \
    Training/Managers/multigametrainingmanager.cpp \
    Training/Managers/singlegametrainingmanager.cpp \
    Training/abstractgamemanager.cpp \
    Training/abstracttrainablefactory.cpp \
    Training/genetictrainer.cpp \
    Training/opponentstrainer.cpp \
    Training/trainable.cpp \
    Training/trainingmetrics.cpp \
    NeuralTree/neuraltree.cpp \
    NeuralTree/treeio.cpp \
    NeuralTree/treecondition.cpp \
    NeuralTree/treenode.cpp \
    Entities/entity.cpp \
    Entities/fieldofview.cpp \
    Game/gamemanager.cpp \
    Entities/hider.cpp \
    AI/hidenseekai.cpp \
    AI/hiderai.cpp \
    hiderfactory.cpp \
    main.cpp \
    mainwindow.cpp \
    Entities/movingentity.cpp \
    Entities/seeker.cpp \
    seekerfactory.cpp \
    Training/Managers/trainingmanager.cpp \
    treedisplaywindow.cpp \
    utils/bigtimer.cpp \
    utils/utils.cpp \
    movingaverage.cpp \
    Collision/collisiondetector.cpp

HEADERS += \
    AI/seekerai.h \
    Display/displayarea.h \
    Display/footprintsdisplay.h \
    Display/previewdisplay.h \
    Display/scorehistorydisplay.h \
    Display/scorehistorywidget.h \
    Display/treedisplay.h \
    Entities/eye.h \
    Game/floortile.h \
    Game/room.h \
    MultiGameTraining/multigamefactory.h \
    MultiGameTraining/multigametrainable.h \
    NeuralTree/drawabletreenode.h \
    ScoreHistory/scoredatapoint.h \
    ScoreHistory/scorehistory.h \
    Training/Managers/multigametrainingmanager.h \
    Training/Managers/singlegametrainingmanager.h \
    Training/abstractgamemanager.h \
    Training/abstracttrainablefactory.h \
    Training/genetictrainer.h \
    Training/opponentstrainer.h \
    Training/trainable.h \
    Training/trainingmetrics.h \
    NeuralTree/mutableelement.h \
    NeuralTree/neuraltree.h \
    NeuralTree/treeio.h \
    NeuralTree/treecondition.h \
    NeuralTree/treenode.h \
    Entities/entity.h \
    Entities/fieldofview.h \
    Game/gamemanager.h \
    Entities/hider.h \
    AI/hidenseekai.h \
    AI/hiderai.h \
    hiderfactory.h \
    mainwindow.h \
    Entities/movingentity.h \
    seekerfactory.h \
    Training/Managers/trainingmanager.h \
    treedisplaywindow.h \
    utils/bigtimer.h \
    utils/utils/random.h \
    utils/utils/utils.h \
    Entities/seeker.h \
    utils/utils.h \
    utils/random.h \
    movingaverage.h \
    Collision/collisiondetector.h

FORMS += \
    mainwindow.ui \
    treedisplaywindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
