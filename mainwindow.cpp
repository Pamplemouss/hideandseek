#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    treeWindow = new TreeDisplayWindow(this);

    connect(ui->scoreHistory, &ScoreHistoryWidget::display, ui->liveDisplay, &PreviewDisplay::specificPreview);
    connect(ui->scoreHistory, &ScoreHistoryWidget::backToLive, ui->liveDisplay, &PreviewDisplay::backToLive);

    connect(ui->scoreHistory, &ScoreHistoryWidget::display, treeWindow->getTreeDisplay(), &TreeDisplay::updateTree);
}

MainWindow::~MainWindow()
{
    delete treeWindow;
    delete ui;
}


void MainWindow::on_showBrainBtn_clicked()
{
    treeWindow->show();
}
