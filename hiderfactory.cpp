#include "Game/gamemanager.h"
#include "hiderfactory.h"

#include <AI/hiderai.h>

HiderFactory::HiderFactory()
{

}

Trainable *HiderFactory::create(const QString &name) const
{
    HiderAI *res = new HiderAI(name);
    this->initGame(res);

    return res;
}

Trainable *HiderFactory::create(const Trainable &trainable)const
{
    HiderAI *res = new HiderAI(dynamic_cast<const HiderAI&>(trainable));
    this->initGame(res);
    return res;
}

Trainable *HiderFactory::copy(const Trainable &trainable) const
{
    qFatal("Not  implemented");
}

void HiderFactory::remove(Trainable *t) const
{
    HiderAI *hider = dynamic_cast<HiderAI*>(t);
    GameManager *g(hider->getGameManager());
    qDeleteAll(g->getHiders());
    qDeleteAll(g->getSeekers());
    delete g;
}

void HiderFactory::setOpponents(const QVector<Trainable *> &value)
{
    qDeleteAll(opponents);
    opponents.clear();
    for(auto *v : value){
        opponents.append(new SeekerAI(*dynamic_cast<SeekerAI*>(v)));
    }
}


void HiderFactory::initGame(HiderAI *newHider)const
{
    GameManager *g = new GameManager();

    // deep copy opponents
    QVector<MovingEntity *> copiedOpponents;
    for(auto *o : opponents){
        SeekerAI *m = new SeekerAI(*dynamic_cast<SeekerAI*>(o));
        copiedOpponents.append(m);
    }
    // notify game manager of his new players
    g->setHiders({newHider});
    g->setSeekers(copiedOpponents);

    g->restart();
}

int HiderFactory::getDesiredOpponentCount() const
{
    return 1;
}
