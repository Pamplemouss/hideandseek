#include "hidenseekai.h"
#include "Game/gamemanager.h"

HideNSeekAI::HideNSeekAI(const QString &name):
    avgScore(1),
    inputs(new QVector<TreeIO>(2)), // TODO: init with the right number of inputs/outputs
    outputs(new QVector<TreeIO>(3))
{
    QVector<QString> outNames({"LEFT", "STRAIT", "RIGHT", "STAND"});
    int namesI=0;
    for(int i=0;i<outputs->size();i++){

        (*outputs)[i] = TreeIO(TreeIO::CONSTANT, double(i), outNames[namesI++]);
    }

    brain = new NeuralTree(inputs, outputs, name);
    brain->setTargetSize(10);
    setInputsSize({"?", "?"});
}

HideNSeekAI::HideNSeekAI(const HideNSeekAI &ai):
    inputs(new QVector<TreeIO>(*ai.inputs)),
    outputs(new QVector<TreeIO>(*ai.outputs)),
    brain(new NeuralTree(*ai.brain)),
    avgScore(ai.avgScore),
    realInputsSize(ai.realInputsSize)
{
    brain->setInputs(inputs);
    brain->setActions(outputs);
}

HideNSeekAI::~HideNSeekAI()
{
    delete brain;
    delete inputs;
    delete outputs;
}

void HideNSeekAI::resetBestScore()
{
    Trainable::resetBestScore();
    avgScore.clear();
}


void HideNSeekAI::setInputsSize(const QVector<QString> &names)
{

    realInputsSize=names.size();
    inputs->clear();
    for(int i=0;i<realInputsSize;i++){
        inputs->append(TreeIO(TreeIO::INPUT, 0.0, names[i]));
    }
    for(double i=0.1;i<=1.0;i*=2.0){
        double value = 1.0-i;

        inputs->append(TreeIO(TreeIO::CONSTANT, value, "CONST(" + QString::number(value,'g', 2) + ")"));
    }
    brain->setInputs(inputs);
}

NeuralTree *HideNSeekAI::getBrain() const
{
    return brain;
}


void HideNSeekAI::mutate()
{
    brain->setTargetSize(qMin(qMax(brain->getLength()+1, 20), 200));

    this->brain->mutate();
}

unsigned long long HideNSeekAI::hash() const
{
    return brain->hash();
}

void HideNSeekAI::copyBrain(const HideNSeekAI *other)
{
    delete brain;
    brain = new NeuralTree(*other->brain);
}


/*
double HideNSeekAI::getScore() const
{
    return avgScore.getCurrentAverage();
}

void HideNSeekAI::setScore(double value)
{
    avgScore.add(value);

    if(value>getBestScore()){
        bestScore=value;
    }
}

*/
