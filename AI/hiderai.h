#ifndef HIDERAI_H
#define HIDERAI_H

#include "hidenseekai.h"
#include "Entities/hider.h"



class HiderAI: public HideNSeekAI, public Hider
{
public:
    HiderAI(const QString &name);
    HiderAI(const HiderAI &hider);
    virtual ~HiderAI();

    void playWholeRun();
    void stepItself();
    double getBestPossibleScore()const;

    void draw(QPainter *p, int width, int height);
    void step();
    void reset();

    // MovingEntity interface
public:
    void initInputs(int nAdversaries);
};

#endif // HIDERAI_H
