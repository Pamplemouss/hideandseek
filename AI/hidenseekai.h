#ifndef HIDENSEEKAI_H
#define HIDENSEEKAI_H


#include "Entities/movingentity.h"
#include <NeuralTree/neuraltree.h>
#include <Training/trainable.h>
#include "movingaverage.h"

class HideNSeekAI: public Trainable
{
public:
    HideNSeekAI(const QString &name);
    HideNSeekAI(const HideNSeekAI &ai);
    virtual ~HideNSeekAI();
    void resetBestScore() override;
    void setInputsSize(const QVector<QString> &names);
    void setInputsNames(const QVector<QString> &names);

protected:
    QVector<TreeIO> *inputs;
    QVector<TreeIO> *outputs;
    NeuralTree *brain;
    MovingAverage avgScore;
    int realInputsSize;

    // Trainable interface
public:
    void mutate();
    unsigned long long hash() const;
    void copyBrain(const HideNSeekAI *other);
    NeuralTree *getBrain() const;

};

#endif // HIDENSEEKAI_H
