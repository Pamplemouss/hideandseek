#ifndef SEEKERAI_H
#define SEEKERAI_H

#include "hidenseekai.h"
#include "Entities/seeker.h"


class SeekerAI: public HideNSeekAI, public Seeker
{
public:
    SeekerAI(const QString &name);
    SeekerAI(const SeekerAI &s);
    virtual ~SeekerAI();
    void playWholeRun();
    void stepItself();
    double getBestPossibleScore()const;


    void draw(QPainter *p, int width, int height);
    void step();
    void reset();

    // MovingEntity interface
public:
    void initInputs(int nAdversaries);
};

#endif // SEEKERAI_H
