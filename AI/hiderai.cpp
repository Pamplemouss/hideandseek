#include "hiderai.h"
#include "Game/gamemanager.h"

HiderAI::HiderAI(const QString &name):
    HideNSeekAI(name),
    Hider()
{

}

HiderAI::HiderAI(const HiderAI &hider):
    HideNSeekAI(hider),
    Hider(hider)
{

}

HiderAI::~HiderAI()
{

}

void HiderAI::playWholeRun()
{
    gameManager->restart();

    while (!gameManager->isGameOver()) {
        gameManager->step();
    }
    gameManager->updateScores();
}

void HiderAI::stepItself()
{



    const auto &newInputs(makeInputs());
    // prevent uninitialized inputs errors
    Q_ASSERT_X(newInputs.size() == realInputsSize, "void HiderAI::step()", "Inputs size are different !");

    for(int i=0;i<newInputs.size();i++){
        (*inputs)[i].setValue(newInputs[i]);
    }

    double decision = brain->passForward();

    this->go();
    if(decision == 0.0){
        this->turn(-10);
    }
    else if(decision == 1.0){
       //this->turn(0);
    }
    else if(decision == 2.0){
        this->turn(10);
    }
    /*
    else if(decision== 3.0){
        this->stand();
    }*/
    else{
        qFatal("Unknown action");
    }
    Hider::stepItself();
}

double HiderAI::getBestPossibleScore() const
{
    return 85.0;
}

void HiderAI::draw(QPainter *p, int width, int height)
{
    gameManager->draw(p);
}

void HiderAI::step()
{
    if(gameManager->isGameOver()){
        gameManager->restart();
    }

    gameManager->step();
}

void HiderAI::reset()
{
    gameManager->restart();
}

void HiderAI::initInputs(int nAdversaries)
{
    MovingEntity::initInputs(nAdversaries);
    this->setInputsSize(getInputsNames());
}


