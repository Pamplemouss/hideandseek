#include "seekerai.h"
#include "Game/gamemanager.h"

#include <QString>

SeekerAI::SeekerAI(const QString &name):
    HideNSeekAI(name),
    Seeker()
{

}

SeekerAI::SeekerAI(const SeekerAI &s):
    HideNSeekAI(s),
    Seeker(s)
{

}

SeekerAI::~SeekerAI()
{

}

void SeekerAI::playWholeRun()
{
    gameManager->restart();

    while (!gameManager->isGameOver()) {
        gameManager->step();
    }

    gameManager->updateScores();
}

void SeekerAI::stepItself()
{



    const auto &newInputs(makeInputs());

    Q_ASSERT_X(newInputs.size() == realInputsSize, "void SeekerAI::step()", "Inputs size are different !");

    for(int i=0;i<newInputs.size();i++){
        (*inputs)[i].setValue(newInputs[i]);
    }


    double decision = brain->passForward();
    this->go();

    if(decision == 0.0){
        this->turn(-10);
    }
    else if(decision == 1.0){
       // this->turn(0);
    }
    else if(decision == 2.0){
        this->turn(10);
    }
    /*
    else if(decision== 3.0){
        this->stand();
    }
    */
    else{
        qFatal("Unknown action");
    }

    Seeker::stepItself();

}

double SeekerAI::getBestPossibleScore() const
{
    return 90.0;
}

void SeekerAI::draw(QPainter *p, int width, int height)
{
    gameManager->draw(p);
}

void SeekerAI::step()
{
    if(gameManager->isGameOver()){
        gameManager->restart();
    }

    gameManager->step();
}

void SeekerAI::reset()
{
    gameManager->restart();
}

void SeekerAI::initInputs(int nAdversaries)
{
    MovingEntity::initInputs(nAdversaries);
    this->setInputsSize(getInputsNames());
}



