#include "trainable.h"

Trainable::Trainable():
    score(-1000),
    bestScore(-1000),
    generation(0)
{

}

Trainable::Trainable(const Trainable &t):
    score(-1000),
    bestScore(-1000),
    generation(t.generation)
{

}

bool Trainable::ptrCompare(Trainable *a, Trainable *b)
{
    return ((*a)<(*b));
}

bool Trainable::operator<(const Trainable &b)
{
    return  this->getScore() < b.getScore();
}

double Trainable::getScore() const
{
    return score;
}

void Trainable::setScore(double value)
{
    score = value;
    if(score>bestScore){
        bestScore=score;
    }
}

double Trainable::getBestScore() const
{
    return bestScore;
}

void Trainable::resetBestScore()
{
    bestScore=-1000;
}

void Trainable::decreaseBestScore(double n)
{
    bestScore-=n;
}

int Trainable::getGeneration() const
{
    return generation;
}

void Trainable::incGeneration()
{
    generation++;
}

void Trainable::setGeneration(int value)
{
    generation = value;
}
