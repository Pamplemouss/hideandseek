#ifndef ABSTRACTGAMEMANAGER_H
#define ABSTRACTGAMEMANAGER_H

#include <QPainter>

class AbstractGameManager
{
public:
    AbstractGameManager();
    virtual ~AbstractGameManager();

    virtual void step()=0;
    virtual void draw(QPainter *painter)const=0;    
    virtual bool isGameOver()=0;


};

#endif // ABSTRACTGAMEMANAGER_H
