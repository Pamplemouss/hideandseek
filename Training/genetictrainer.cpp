#include "genetictrainer.h"
#include <QtMath>
#include "Training/trainable.h"
#include "utils/random.h"
#include "Training/abstracttrainablefactory.h"

#define ENABLE_MULTITHREADING

int GeneticTrainer::nameCounter=0;

GeneticTrainer::GeneticTrainer(AbstractTrainableFactory *factory, int poolSize, double bestProportion):
    poolSize(poolSize),
    bestProportion(bestProportion),
    metrics(QString('A' + nameCounter++)),
    factory(factory),
    generationToKeep(10)
{
    lastAllTimeHigh=-1000;
    for(int i=0;i<poolSize;i++){
        pool.push_back(this->factory->create(metrics.getName()));
    }
    //threadPool.setMaxThreadCount(4);
}

GeneticTrainer::~GeneticTrainer()
{
    for(int i=0;i<pool.size();i++){
        factory->remove(pool[i]);
    }
}

void GeneticTrainer::train()
{

    if(pool.size()<poolSize){
        repopulate();
    }

    int threadCount = threadPool.maxThreadCount();
    int chunkSize = qCeil(double(pool.size())/threadCount/4.0);

#ifdef ENABLE_MULTITHREADING
    for(int i=0;i<pool.size();i+=chunkSize){
        threadPool.start(new RangeWorker(this, &GeneticTrainer::playWholeRun, i, (i+chunkSize)), QThread::Priority::HighestPriority);
    }
#else


    for(int i=0;i<pool.size();i++){
        pool[i]->playWholeRun();
    }
#endif

    while(!threadPool.waitForDone()){
        QThread::usleep(1);
    }

    std::sort(pool.rbegin(), pool.rend(), Trainable::ptrCompare);

    metrics.addTrainingCycle();

    /// only update generation to keep  at end of training
    generationToKeep=newGenerationToKeep;

    emit trainingDone();
    selectBest();
}

void GeneticTrainer::resetAllScores()
{
    for(auto *p : pool){
        p->resetBestScore();
    }
    for(auto *p : allTimeHighPool){
        p->resetBestScore();
        // decrease all time high best score in order to make the best entities older, and remove them over time if they don't perform well
        //p->decreaseBestScore(-3.0);
    }
    //allTimeHighPool.clear();
}

void GeneticTrainer::selectBest()
{
    QVector<Trainable*> newPool;

    int elementsToKeep = qCeil((poolSize)*bestProportion);
    /*for(int i=0;i<pool.size();i++){
        pool[i]->getGame().setFitness(pool[i]->getGame().getFitness());
    }*/
    // sort AI by fitness
    std::sort(pool.rbegin(), pool.rend(), Trainable::ptrCompare);


    for(int i=0;i<pool.size();i++){

        if(i<elementsToKeep){
            addToAllTimeHigh(pool[i]);

            if(!allTimeHighPool.contains(pool[i])){
                newPool.push_back(pool[i]);
            }
        }else if(pool[i]->getGeneration()<generationToKeep){
            newPool.push_back(pool[i]);
        }
    }
    for(int i=elementsToKeep;i<pool.size();i++){
        if(pool[i]->getGeneration()>=generationToKeep && !allTimeHighPool.contains(pool[i])){
            factory->remove(pool[i]);
        }
    }


    for(int i=0;i<allTimeHighPool.size();i++){
        newPool.append(allTimeHighPool[i]);
    }

    pool = newPool;

}

void GeneticTrainer::repopulate()
{
    QVector<Trainable*> newPool;
    Random r;
        int childPerSurvivor = qMax(qCeil(double(poolSize)/bestProportion/double(poolSize)) - 1, 1);
        for(int i=0;i<pool.size();i++){
            for(int j=0;j<childPerSurvivor;j++){
                Trainable* newBot = factory->create(*pool[i]);

                Q_ASSERT_X(newBot->hash()==pool[i]->hash(), "void GeneticTrainer::repopulate()", "Hash code are not equals");

                // do actual mutation
                newBot->mutate();

                newPool.push_back(newBot);
            }
            // create a copy of current good performing bot, in order to update its opponents
            Trainable *t=factory->create(*pool[i]);
            newPool.push_back(t);

            // update new copy in training pool
            if(allTimeHighPool.contains(pool[i])){
                int index = allTimeHighPool.indexOf(pool[i]);

                //t->setScore(allTimeHighPool[index]->getScore());
                allTimeHighPool[index] = t;
            }

            // remove old one
            factory->remove(pool[i]);
        }

    pool = newPool;

}

QVector<Trainable *> GeneticTrainer::getPool() const
{
    return pool;
}

QVector<Trainable *> GeneticTrainer::getBest()
{
    return QVector<Trainable*>({this->getFirst()});
}

bool GeneticTrainer::addToAllTimeHigh(Trainable *b)
{
    bool added=false;
    if(allTimeHighPool.size()<(poolSize * bestProportion)/4.0 && !allTimeHighPool.contains(b)){
        allTimeHighPool.push_back(b);
        added=true;
    }
    else{
        for(int i=0;i<allTimeHighPool.size();i++){
            if(b->getBestScore() > allTimeHighPool[i]->getBestScore() && !allTimeHighPool.contains(b)){
                //delete allTimeHighPool[i];
                //allTimeHighPool[i] = new Trainable(*b);

                // reset generation for agents that perform best
                b->setGeneration(0);
                allTimeHighPool[i] = b;
                //allTimeHighPool[i] = factory.create(*b);
                //allTimeHighPool[i]->setScore(b->getScore());

                added=true;
                if(lastAllTimeHigh<b->getBestScore()){
                    lastAllTimeHigh=b->getBestScore();
                }
                break;
            }
        }
    }

    //Q_ASSERT_X(lastAllTimeHigh<=allTimeHighPool.first()->getBestScore(), "", "Last all time high is lower");

    if(added){
           std::sort(allTimeHighPool.rbegin(), allTimeHighPool.rend(), Trainable::ptrCompare);
    }
    return added;
}

Trainable *GeneticTrainer::findRandomBot(const Trainable *exclude) const
{
    Trainable *res;
    Random random;
    do{
        res = pool[random.i(pool.size())];
    }while(res==exclude);

    return res;
}


void GeneticTrainer::newTraining()
{
    train();
}

AbstractTrainableFactory *GeneticTrainer::getFactory()
{
    return factory;
}

int GeneticTrainer::getGenerationToKeep() const
{
    return newGenerationToKeep;
}

void GeneticTrainer::setGenerationToKeep(int value)
{
    newGenerationToKeep = value;
}

TrainingMetrics GeneticTrainer::getMetrics()
{
    //Trainable *b = getFirst();
    // TODO : Create & return abstract metrics
    //metrics.setBrainSize(b->getLength());
    //metrics.setTargetBrainSize(b->getBrain()->getTargetSize());
    return metrics;
}

void GeneticTrainer::updateMetrics()
{
    //Trainable *b = getFirst();
    // TODO : Create & return abstract metrics
    //b->getBrain()->refreshComposition();
    //metrics.setComposition(b->getBrain()->getComposition());
    metrics.setMaxScore(getFirst()->getScore());
}

Trainable *GeneticTrainer::getFirst()
{


    Trainable *b;

    if(allTimeHighPool.size()>0){
        std::sort(allTimeHighPool.rbegin(), allTimeHighPool.rend(), Trainable::ptrCompare);
        b=allTimeHighPool.first();
    }
    else{
        std::sort(pool.rbegin(), pool.rend(), Trainable::ptrCompare);
        b=pool.first();
    }

    return b;
}

void GeneticTrainer::playWholeRun(int i)
{
    if(i>0 && i<pool.size()){
        pool[i]->playWholeRun();
        pool[i]->incGeneration();
    }
}

QVector<Trainable *> GeneticTrainer::getAllTimeHighPool() const
{
    return allTimeHighPool;
}

void GeneticTrainer::addToPool(Trainable *bot)
{
    pool.append(bot);
}
