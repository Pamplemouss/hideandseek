#ifndef TRAINABLEFACTORY_H
#define TRAINABLEFACTORY_H

#include <QString>
#include <QVector>


class Trainable;

class AbstractTrainableFactory
{
public:
    virtual ~AbstractTrainableFactory(){}
    virtual Trainable *create(const QString &name)const=0;
    virtual Trainable *create(const Trainable &trainable)const=0;
    virtual Trainable *copy(const Trainable &trainable)const=0;
    virtual void remove(Trainable *t)const=0;
    virtual QVector<Trainable *> getOpponents() const;
    virtual void setOpponents(const QVector<Trainable *> &value)=0;
    virtual int getDesiredOpponentCount()const=0;

    int generateId();
protected:
    int counter=0;
    QVector<Trainable *> opponents;
};

#endif // TRAINABLEFACTORY_H
