#ifndef OPPONENTSTRAINER_H
#define OPPONENTSTRAINER_H

#include "genetictrainer.h"




class OpponentsTrainer: public QThread
{
    Q_OBJECT
public:
    OpponentsTrainer(GeneticTrainer *trainer1, GeneticTrainer *trainer2,int cyclesPerTrainer=1);
    virtual ~OpponentsTrainer();

signals:
    void newBestScore(Trainable* trainable);

private:
    GeneticTrainer *trainer1;
    GeneticTrainer *trainer2;
    int cyclesPerTrainer;

    void train(GeneticTrainer *trainer, int cycles);
    QVector<Trainable*> generateOpponents(GeneticTrainer *trainer);
    // QThread interface
protected:
    void run();

    QVector<Trainable*> history1;
    QVector<Trainable*> history2;
};

#endif // OPPONENTSTRAINER_H
