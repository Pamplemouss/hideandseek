#include "trainingmetrics.h"


TrainingMetrics::TrainingMetrics(const QString &name):
    name(name),
    maxScore(-999),
    trainingCycle(0),
    averageTreeSize(1.0),
    brainSize(0)
{

}

double TrainingMetrics::getMaxScore() const
{
    return maxScore;
}

void TrainingMetrics::setMaxScore(double value)
{
 //   if(value>maxScore){
        maxScore = value;
   // }
}

void TrainingMetrics::draw(QPainter *painter, int size, int offsetX, int offsetY)
{
    QPen drawingPen;
    QPen rectPen;
    painter->setBrush(QBrush());
    rectPen.setWidth(0);

    drawingPen.setWidth(2);
    drawingPen.setColor(Qt::gray);
    painter->setPen(drawingPen);
    painter->drawRect(offsetX,offsetY, size,  size);

    painter->drawText(offsetX, offsetY+10, "Name  : " + name);
    painter->drawText(offsetX, offsetY+20, "Score : " +QString::number(maxScore));
    painter->drawText(offsetX, offsetY+30, "Cycles: " +QString::number(trainingCycle));
    painter->drawText(offsetX, offsetY+40, "Size  : " +QString::number(brainSize) + " | target: " + QString::number(targetBrainSize));
    painter->drawText(offsetX, offsetY+60, "DNA   : ");
    QHashIterator<QString, double> i(composition);
    int offset=0;
    while(i.hasNext()){
        i.next();
        painter->drawText(offsetX+15, offsetY+70 + offset, i.key() + ": " +QString::number(i.value()*100.0) + "%");
        offset+=10;
    }
}

int TrainingMetrics::getTrainingCycle() const
{
    return trainingCycle;
}

void TrainingMetrics::addTrainingCycle()
{
    trainingCycle++;
}

QString TrainingMetrics::getName() const
{
    return name;
}

QHash<QString, double> TrainingMetrics::getComposition() const
{
    return composition;
}

void TrainingMetrics::setComposition(const QHash<QString, double> &value)
{
    composition = value;
}

int TrainingMetrics::getBrainSize() const
{
    return brainSize;
}

void TrainingMetrics::setBrainSize(int value)
{
    brainSize = value;
}

int TrainingMetrics::getTargetBrainSize() const
{
    return targetBrainSize;
}

void TrainingMetrics::setTargetBrainSize(int value)
{
    targetBrainSize = value;
}
