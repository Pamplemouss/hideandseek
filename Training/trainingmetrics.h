#ifndef TRAININGMETRICS_H
#define TRAININGMETRICS_H

#include <QPainter>
#include <QString>



class TrainingMetrics
{
public:
    TrainingMetrics(const QString &name);

    double getMaxScore() const;
    void setMaxScore(double value);

    void draw(QPainter *painter, int size, int offsetX, int offsetY);

    int getTrainingCycle() const;

    void addTrainingCycle();

    QString getName() const;

    QHash<QString, double> getComposition() const;
    void setComposition(const QHash<QString, double> &value);

    int getBrainSize() const;
    void setBrainSize(int value);

    int getTargetBrainSize() const;
    void setTargetBrainSize(int value);

private:
    QString name;
    double maxScore;
    int trainingCycle;
    double averageTreeSize;
    QHash<QString, double> composition;
    int brainSize;
    int targetBrainSize;

};

#endif // TRAININGMETRICS_H
