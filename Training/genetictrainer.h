#ifndef GENETICTRAINER_H
#define GENETICTRAINER_H

#include "trainingmetrics.h"

#include <QObject>
#include <QThreadPool>
#include <QVector>

#include "abstracttrainablefactory.h"

class Trainable;

class GeneticTrainer: public QObject
{
    Q_OBJECT


public:
    GeneticTrainer(AbstractTrainableFactory* factory, int poolSize, double bestProportion);
    virtual ~GeneticTrainer();
    void train();
    void resetAllScores();
    void replayBest();
    void selectBest();
    void repopulate();
    QVector<Trainable *> getPool() const;
    QVector<Trainable*> getBest();
    bool addToAllTimeHigh(Trainable* b);
    Trainable *findRandomBot(const Trainable* exclude=nullptr)const;

    QVector<Trainable *> getAllTimeHighPool() const;
    void addToPool(Trainable *bot);

    static int nameCounter;

    TrainingMetrics getMetrics();
    void updateMetrics();
    Trainable *getFirst();

    void playWholeRun(int i);

    int getGenerationToKeep() const;
    void setGenerationToKeep(int value);

    AbstractTrainableFactory *getFactory();

public slots:
    void newTraining();

signals:
    void trainingDone();

private:
    void doSelection();

    int poolSize;
    double bestProportion;
    QVector<Trainable*> pool;
    QVector<Trainable*> allTimeHighPool;
    QThreadPool threadPool;
    TrainingMetrics metrics;
    double lastAllTimeHigh;
    AbstractTrainableFactory* factory;
    int generationToKeep;
    int newGenerationToKeep;

    class RangeWorker: public QRunnable
    {
        typedef  void (GeneticTrainer::*CallbackPtr)(int n);

        public:
            RangeWorker(GeneticTrainer *parent, CallbackPtr callback, int start, int stop):
                parent(parent),
                callback(callback),
                start(start),
                stop(stop){
            }
            virtual ~RangeWorker(){}
            void run(){
                for(int i=start;i<stop;i++){
                    // call callback method
                    (parent->*callback)(i);
                }
            }

        private:
            GeneticTrainer *parent;
            CallbackPtr callback;
            int start;
            int stop;
    };

};

#endif // GENETICTRAINER_H
