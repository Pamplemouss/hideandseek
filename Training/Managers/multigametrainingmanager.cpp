#include "multigametrainingmanager.h"

#include <MultiGameTraining/multigamefactory.h>

#include <AI/hiderai.h>

MultigameTrainingManager::MultigameTrainingManager()
{

    hiderFactory = new MultigameFactory<HiderAI, SeekerAI>();
    seekerFactory = new MultigameFactory<SeekerAI, HiderAI>();

    SeekerAI *bestSeeker = new SeekerAI("InitialSeeker");
    GameManager *g1(new GameManager());
    g1->setSeekers({bestSeeker});
    hiderFactory->setOpponents({bestSeeker}); // init opponents with a basic untrained one
    g1->restart();

    HiderAI *bestHider = new HiderAI("InitialHider");
    GameManager *g2(new GameManager());
    g2->setHiders({bestHider});
    seekerFactory->setOpponents({bestHider}); // init opponents with a basic untrained one
    g2->restart();

    init();
}

Trainable *MultigameTrainingManager::getBestHider() const
{
   MultiGameTrainable<HiderAI>* multiGame = dynamic_cast<MultiGameTrainable<HiderAI>*>(hidersTrainer->getFirst());
   return multiGame->getAI();
}

Trainable *MultigameTrainingManager::getBestSeeker() const
{
    MultiGameTrainable<SeekerAI>* multiGame = dynamic_cast<MultiGameTrainable<SeekerAI>*>(seekersTrainer->getFirst());
    return multiGame->getAI();
}

AbstractTrainableFactory *MultigameTrainingManager::getFactory(const Trainable &trainable) const
{
    if(dynamic_cast<const MultiGameTrainable<SeekerAI>*>(&trainable)){
        return getSeekerFactory();
    }
    else if(dynamic_cast<const MultiGameTrainable<HiderAI>*>(&trainable)){
        return getHiderFactory();
    }
    else{
        qFatal("SinglegameTrainingManager::getFactory() : unknown trainable");
    }
}
