#ifndef SINGLEGAMETRAININGMANAGER_H
#define SINGLEGAMETRAININGMANAGER_H

#include "trainingmanager.h"

class SinglegameTrainingManager: public TrainingManager
{
    public:
    SinglegameTrainingManager();
    AbstractTrainableFactory *getFactory(const Trainable &trainable)const;
    // TrainingManager interface
public:
    Trainable *getBestHider() const;
    Trainable *getBestSeeker() const;
};

#endif // SINGLEGAMETRAININGMANAGER_H
