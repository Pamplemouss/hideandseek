#ifndef MULTIGAMETRAININGMANAGER_H
#define MULTIGAMETRAININGMANAGER_H

#include "trainingmanager.h"



class MultigameTrainingManager : public TrainingManager
{
public:
    MultigameTrainingManager();

    // TrainingManager interface
public:
    Trainable *getBestHider() const;
    Trainable *getBestSeeker() const;
    AbstractTrainableFactory *getFactory(const Trainable &trainable) const;
};

#endif // MULTIGAMETRAININGMANAGER_H
