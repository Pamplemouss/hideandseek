#include "multigametrainingmanager.h"
#include "singlegametrainingmanager.h"
#include "trainingmanager.h"

#include <Game/gamemanager.h>

#include <AI/hiderai.h>

#include <Training/genetictrainer.h>

TrainingManager *TrainingManager::instance=nullptr;

TrainingManager::TrainingManager()
{


}

void TrainingManager::init()
{
    const int poolSize = 50;
    const double keep = 0.01;

    scoreHistory = new ScoreHistory();

    // init genetic trainers
    hidersTrainer = new GeneticTrainer(hiderFactory, poolSize, keep);
    hidersTrainer->setGenerationToKeep(0);
    seekersTrainer = new GeneticTrainer(seekerFactory, poolSize, keep);
    seekersTrainer->setGenerationToKeep(0);

    // init OpponentsTrainer using genetic trainers
    trainer = new OpponentsTrainer(hidersTrainer, seekersTrainer, 500000);


    // score history connection
    connect(seekersTrainer, &GeneticTrainer::trainingDone, scoreHistory, &ScoreHistory::addSeekerScore, Qt::BlockingQueuedConnection);
    connect(hidersTrainer, &GeneticTrainer::trainingDone, scoreHistory, &ScoreHistory::addHiderScore, Qt::BlockingQueuedConnection);
}

ScoreHistory *TrainingManager::getScoreHistory() const
{
    return scoreHistory;
}

AbstractTrainableFactory *TrainingManager::getSeekerFactory() const
{
    return seekerFactory;
}

AbstractTrainableFactory *TrainingManager::getHiderFactory() const
{
    return hiderFactory;
}

OpponentsTrainer *TrainingManager::getTrainer() const
{
    return trainer;
}

GeneticTrainer *TrainingManager::getSeekersTrainer() const
{
    return seekersTrainer;
}

GeneticTrainer *TrainingManager::getHidersTrainer() const
{
    return hidersTrainer;
}


TrainingManager *TrainingManager::getInstance()
{
    if(instance==nullptr){
        instance = new MultigameTrainingManager();
    }

    return instance;
}

