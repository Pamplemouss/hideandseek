#include "singlegametrainingmanager.h"

#include <Game/gamemanager.h>

#include <AI/hiderai.h>

SinglegameTrainingManager::SinglegameTrainingManager()
{

    hiderFactory = new HiderFactory();
    seekerFactory = new SeekerFactory();

    SeekerAI *bestSeeker = new SeekerAI("InitialSeeker");
    GameManager *g1(new GameManager());
    g1->setSeekers({bestSeeker});
    hiderFactory->setOpponents({bestSeeker}); // init opponents with a basic untrained one
    g1->restart();

    HiderAI *bestHider = new HiderAI("InitialHider");
    GameManager *g2(new GameManager());
    g2->setHiders({bestHider});
    seekerFactory->setOpponents({bestHider}); // init opponents with a basic untrained one
    g2->restart();

    init();
}

AbstractTrainableFactory *SinglegameTrainingManager::getFactory(const Trainable &trainable) const
{
    if(dynamic_cast<const Seeker*>(&trainable)){
        return getSeekerFactory();
    }
    else if(dynamic_cast<const Seeker*>(&trainable)){
        return getHiderFactory();
    }
    else{
        qFatal("SinglegameTrainingManager::getFactory() : unknown trainable");
    }
}

Trainable *SinglegameTrainingManager::getBestHider() const
{
    return hiderFactory->create(*hidersTrainer->getFirst());
}

Trainable *SinglegameTrainingManager::getBestSeeker() const
{
    return seekerFactory->create(*seekersTrainer->getFirst());
}
