#ifndef TRAININGMANAGER_H
#define TRAININGMANAGER_H

#include "hiderfactory.h"
#include "seekerfactory.h"
#include "ScoreHistory/scorehistory.h"

#include <Training/genetictrainer.h>
#include <Training/opponentstrainer.h>


class SinglegameTrainingManager;
class Trainable;

class TrainingManager: public QObject
{
    Q_OBJECT

public:
    TrainingManager();
    static TrainingManager *getInstance();

    GeneticTrainer *getHidersTrainer() const;
    GeneticTrainer *getSeekersTrainer() const;

    OpponentsTrainer *getTrainer() const;

    AbstractTrainableFactory *getHiderFactory() const;
    AbstractTrainableFactory *getSeekerFactory() const;

    virtual AbstractTrainableFactory *getFactory(const Trainable &trainable)const=0;

    ScoreHistory *getScoreHistory() const;

    virtual Trainable *getBestHider()const=0;
    virtual Trainable *getBestSeeker()const=0;

protected:


    void init();

    GeneticTrainer *hidersTrainer;
    GeneticTrainer *seekersTrainer;

    OpponentsTrainer *trainer;

    AbstractTrainableFactory *hiderFactory;
    AbstractTrainableFactory *seekerFactory;

    ScoreHistory *scoreHistory;
    static TrainingManager *instance;
};

#endif // TRAININGMANAGER_H
