#ifndef TRAINABLE_H
#define TRAINABLE_H

#include <QPainter>



class Trainable
{
public:
    Trainable();
    Trainable(const Trainable &t);

    virtual ~Trainable(){}

    virtual void draw(QPainter *p, int width, int height)=0;
    virtual void step()=0;
    virtual void reset()=0;

    static bool ptrCompare(Trainable *a, Trainable *b);
    virtual bool operator <(const Trainable &b);

    virtual double getScore() const;
    virtual void setScore(double value);
    virtual double getBestScore() const;
    virtual void resetBestScore();
    virtual void decreaseBestScore(double n);

    // game or AI specific, must be reimplemented
    virtual void playWholeRun()=0;
    virtual void mutate()=0;

    virtual unsigned long long hash()const=0;

    virtual int getGeneration() const;
    virtual void incGeneration();

    virtual void setGeneration(int value);
    // best score a trainable may achieve
    virtual double getBestPossibleScore()const =0;

protected:
    double score;
    double bestScore;
    int generation;

};

#endif // TRAINABLE_H
