#include "opponentstrainer.h"
#include "Training/trainable.h"
#include "Game/room.h"

#include <utils/bigtimer.h>

#include <Training/Managers/trainingmanager.h>

OpponentsTrainer::OpponentsTrainer(GeneticTrainer *trainer1, GeneticTrainer *trainer2, int cyclesPerTrainer):
    QThread(),
    trainer1(trainer1),
    trainer2(trainer2),
    cyclesPerTrainer(cyclesPerTrainer)
{

}

OpponentsTrainer::~OpponentsTrainer()
{

}

void OpponentsTrainer::train(GeneticTrainer *trainer, int cycles)
{
    Room::getCombinedFootPrintInstance()->reset();

    int i=0;
    bool isScoreNull = true;

    int perfectScoreAmount=5;

    int perfectScoreCount=perfectScoreAmount;

    const int acceptableScoreDiscount = 20;

    do{
        Room::getCombinedFootPrintInstance()->fadeFootPrint(0.5);
        trainer->train();
        trainer->updateMetrics();
        auto *first = trainer->getFirst();
        // exit loop if we reached maximum score
        double acceptableScore =  first->getBestPossibleScore() - acceptableScoreDiscount;
        if(first->getBestScore() >= acceptableScore){
            perfectScoreCount--;
        }
        else{
            perfectScoreCount=perfectScoreAmount;
        }

        if(perfectScoreCount<=0){
            break;
        }

        if(first->getBestScore()>0.0){
            isScoreNull=false;
        }

        // training cycles count only start after score started to improve
        if(!isScoreNull){
            i++;
        }

        int roomId = Random().i(0xffffff);
        Room::getInstance()->reset(roomId);

        BigTimer::getInstance()->printAverageTime();

    }while (i < cycles);
}


void OpponentsTrainer::run()
{
    TrainingManager *manager = TrainingManager::getInstance();

    while(true){
        // *** train both trainers for n cycles ***

        trainer1->getFactory()->setOpponents(generateOpponents(trainer2));
        train(trainer1, cyclesPerTrainer);

        emit newBestScore(trainer1->getFirst());

        trainer2->getFactory()->setOpponents(generateOpponents(trainer1));
        train(trainer2, cyclesPerTrainer);

        // update best scores (return a copy of best performing entities)
        emit newBestScore(trainer2->getFirst());

        trainer1->resetAllScores();
        trainer2->resetAllScores();

        int roomId = Random().i(0xffffff);
        Room::getInstance()->reset(roomId);
        Room::getCombinedFootPrintInstance()->reset(roomId);
    }
}


QVector<Trainable *> OpponentsTrainer::generateOpponents(GeneticTrainer *trainer)
{
    QVector<Trainable*> res;
    QVector<Trainable*> *history;
    AbstractTrainableFactory *f = trainer->getFactory();


    if(trainer==trainer1){
        history = &history2;
    }
    else{
        history = &history1;
    }

    if(history->size()>=f->getDesiredOpponentCount()){
        f->remove(history->first());
        history->removeFirst();
    }

    history->append(f->create(*trainer->getFirst()));

    for(auto *t : *history){
        res.append(t);
    }
    /*
    AbstractTrainableFactory *f = trainer.getFactory();
    int randomSelect=f->getDesiredOpponentCount() - 1;
    Random random;
    const auto &pool = trainer.getPool();

    for(int i=0;i<randomSelect;i++){
        res.append(pool[random.i(pool.size() - 1) + 1]);
    }
    */

    return res;
}
