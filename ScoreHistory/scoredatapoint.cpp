#include "scoredatapoint.h"

ScoreDatapoint::ScoreDatapoint(Trainable* ai, double seekerScore, double hiderScore, bool seekerTraining)
    :ai(ai), seekerScore(seekerScore), hiderScore(hiderScore), seekerTraining(seekerTraining)
{
}

double ScoreDatapoint::getHiderScore() const
{
    return hiderScore;
}

double ScoreDatapoint::getSeekerScore() const
{
    return seekerScore;
}

bool ScoreDatapoint::wasSeekerTraining() const
{
    return seekerTraining;
}

Trainable *ScoreDatapoint::getAI() const
{
    return ai;
}
