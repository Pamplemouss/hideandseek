#ifndef SCOREDATAPOINT_H
#define SCOREDATAPOINT_H

#include <Training/trainable.h>

class ScoreDatapoint
{
public:
    ScoreDatapoint(Trainable* ai, double seekerScore, double hiderScore, bool seekerTraining);
    double getHiderScore() const;
    double getSeekerScore() const;
    bool wasSeekerTraining() const;
    Trainable *getAI() const;

private:
    Trainable* ai;
    double seekerScore;
    double hiderScore;
    bool seekerTraining;
};

#endif // SCOREDATAPOINT_H
