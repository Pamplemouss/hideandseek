#ifndef SCOREHISTORY_H
#define SCOREHISTORY_H

#include "scoredatapoint.h"
#include "QObject"
#include <QPainter>

class ScoreHistory : public QObject
{
    Q_OBJECT
public:
    ScoreHistory();
    QVector<ScoreDatapoint> getDataset() const;

private:
    QVector<ScoreDatapoint> dataset;

signals:
    void newData(int dataSize);

public slots:
    void addSeekerScore();
    void addHiderScore();
};

#endif // SCOREHISTORY_H
