#include "scorehistory.h"
#include "Training/Managers/trainingmanager.h"
#include "Training/abstracttrainablefactory.h"

ScoreHistory::ScoreHistory()
{
}

QVector<ScoreDatapoint> ScoreHistory::getDataset() const
{
    return dataset;
}

void ScoreHistory::addSeekerScore()
{
    TrainingManager *manager = TrainingManager::getInstance();
    AbstractTrainableFactory *factory = manager->getSeekerFactory();

    Trainable* ai = factory->copy(*manager->getSeekersTrainer()->getFirst());
    double seekerScore = ai->getScore();
    double hiderScore = 100.0 - seekerScore;
    dataset.push_back(ScoreDatapoint(ai, seekerScore, hiderScore, true));

    emit newData(dataset.size());
}

void ScoreHistory::addHiderScore()
{
    TrainingManager *manager = TrainingManager::getInstance();
    AbstractTrainableFactory *factory = manager->getHiderFactory();

    Trainable* ai = factory->copy(*manager->getHidersTrainer()->getFirst());
    double hiderScore = ai->getScore();
    double seekerScore = 100.0 - hiderScore;
    dataset.push_back(ScoreDatapoint(ai, seekerScore, hiderScore, false));

    emit newData(dataset.size());
}
