#ifndef SEEKERFACTORY_H
#define SEEKERFACTORY_H

#include <QString>

#include <Training/abstracttrainablefactory.h>

#include <AI/seekerai.h>


class Trainable;
class SeekerFactory: public AbstractTrainableFactory
{
public:
    SeekerFactory();

    // AbstractTrainableFactory interface
public:
    Trainable *create(const QString &name)const;
    Trainable *create(const Trainable &trainable)const;
    Trainable *copy(const Trainable &trainable) const;

    void remove(Trainable *t)const;

    void setOpponents(const QVector<Trainable *> &value);

    void initGame(SeekerAI *newSeeker) const;
    int getDesiredOpponentCount() const;


};
#endif // SEEKERFACTORY_H
