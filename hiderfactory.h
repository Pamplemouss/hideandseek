#ifndef HIDERFACTORY_H
#define HIDERFACTORY_H

#include <QString>

#include <Training/abstracttrainablefactory.h>

#include <AI/seekerai.h>


class Trainable;
class HiderAI;
class HiderFactory: public AbstractTrainableFactory
{
public:
    HiderFactory();

    // AbstractTrainableFactory interface
public:
    Trainable *create(const QString &name)const;
    Trainable *create(const Trainable &trainable)const;
    Trainable *copy(const Trainable &trainable) const;

    void remove(Trainable *t)const;
    void setOpponents(const QVector<Trainable *> &value);

    void initGame(HiderAI *newHider) const;
    int getDesiredOpponentCount() const;

};

#endif // HIDERFACTORY_H
